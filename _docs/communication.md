---
layout: doc
title: Communication
toc: true
left_menu: true
slug: communication
---

The communication of the different components of the TSG follows the IDS Protocols as basis. Especially the communication between two connectors follows the standards to ensure the interoperability between different IDS connector implementations.

## Overview

There are several points in the different TSG components that use either IDS Protocols or custom protocols for the TSG components. The primary component that combines all of the communication is the Core Container, which acts as gateway for communication between other internal and external components. The figure below shows the main entrypoints of the communication.

Given the figure above, the following communication is distinguished:

1. IDS Information Model message annotated communication.<br />Follows one the following protocols: _MIME Multipart_, _IDSCPv2_, _IDS REST_. See the [inter-connector API section]({{ '/docs/core-container/api#inter-connector-api' | relative_url }}) for a detailed description of the available protocols. </sup>
2. IDS Information Model message annotated communication (not standardized in the IDS standard).<br />The decision is made for the TSG components that the communication between the Core Container and the Data Apps follows the _MIME Multipart_ protocol, this is not standardized in the IDS specification at this moment. See the [Data App API section]({{ '/docs/core-container/api#data-app-api' | relative_url }}) for more information.
3. Flow similar to OpenID Connect, with the difference being the fact that before you send an request to any other connector you first retrieve an Dynamic Attribute Token (DAT) that is signed by the DAPS. With this _DAT_ the receiver can verify the claims inside it as long as it trusts the DAPS that signed it.
4. IDS Information Model message annotated communication combined with Docker Registry interactions for actual retrieval of Docker Images.<br /> Downloading Docker Images will be done outside of the Core Container, to make the usage within Kubernetes not unnecessarily complex. The App Store features are not fully developed at this moment.
5. IDS Information Model message annotated communication.<br />Not fully standardized and supported by the TSG.
6. Core Container administration API. See the full documentation at the [Admin API section]({{ '/docs/core-container/api#admin-api' | relative_url }}).

## Relevant repositories

The relevant public repositories for the IDS Protocols:

* [TSG Organization](https://gitlab.com/tno-tsg) containing all of the TSG components that are using the IDS Protocols.
* [IDS-G](https://github.com/International-Data-Spaces-Association/IDS-G) containing the public IDS specification. [IDS-G Pre](https://github.com/International-Data-Spaces-Association/IDS-G-pre) & [IDS ThinkTank](https://github.com/International-Data-Spaces-Association/IDS-ThinkTank) provide input for the IDS-G repository, these repositories can only be accessed if you're an IDSA member.
* [Information Model](https://github.com/International-Data-Spaces-Association/InformationModel) containing the IDS Information Model ontology
* [Information Model Java library](https://github.com/International-Data-Spaces-Association/Java-Representation-of-IDS-Information-Model) containing the Java library for (de)serialization of the IDS Information Model
* [IDSCPv2 Java library](https://github.com/International-Data-Spaces-Association/idscp2-jvm)
