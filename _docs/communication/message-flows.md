---
layout: doc
title: Message Flows
toc: true
left_menu: true
categories: communication
slug: message-flows
---

The message flows of the TSG components follow the IDS standard with the use of the IDS Information Model and message patterns described in the standard. This page shows a couple of examples of the most used message flows that cover the majority of interactions done by the TSG components.

## Overview

The message flows selected in this documentation are the following:

* **Self Description interaction**: For retrieving the self description of a known connector without the usage of the IDS Metadata Broker.
* **Broker interaction**: For interacting with the IDS Metadata Broker, both publishing self-descriptions and querying the Metadata Broker.
* **Artifact interaction**: For interacting with the built-in artifact handling of the Core Container.
* **Data App interaction**: For interaction between two connectors with Data Apps configured for handling messages.
* **Policy Negotiation**: For interaction with the Policy Negotiation process in the Core Container.
* **DAPS interaction**: For retrieval of Dynamic Attribute Tokens (DATs) that are required for communication between two IDS Connectors.

These message flows are made specific for the TSG components, but follow the IDS standard that describes how two IDS Connectors exchange information between each other. The described flows are rather generic, especially the Data App interaction, that allow a lot of configurability and specialization to fit a wide variety of use cases.

## Self Description

A Self Description encapsulates information about the IDS connector itself and its capabilities and characteristics. For more info see [here](https://docs.internationaldataspaces.org/ids-knowledgebase/v/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer/3_5_4_metadata_broker).

In the TSG, a self description is generated for each Connector. This self description follows the standards set by the IDS Information model and consists of:

- The component ID
- The name of the operator of the component
- A cryptographic hash of the component certificate
- Data endpoints offered by the component
- Security profile of the component
<!-- - Log format of the data endpoints that are offered -->

An example of a self description can be found below. It is a Self Description of a Test Connector in our Playground environment.

<details>
<summary>Click to expand example</summary>
<div markdown="1">

~~~ json
{
  "@context": {
    "ids": "https://w3id.org/idsa/core/",
    "idsc": "https://w3id.org/idsa/code/"
  },
  "@type": "ids:TrustedConnector",
  "@id": "urn:playground:tsg:connectors:TestConnector",
  "ids:publicKey": {
    "@type": "ids:PublicKey",
    "@id": "https://w3id.org/idsa/autogen/publicKey/f62d078b-6c4b-4e85-b820-c2215652e773",
    "ids:keyType": {
      "@id": "https://w3id.org/idsa/code/RSA"
    },
    "ids:keyValue": "MIIEZzCCA0+gAwIBAgIBADANBgkqhkiG9w0BAQsFADCBkzELMAkGA1UEBhMCTkwxEjAQBgNVBAgTCUdyb25pbmdlbjESMBAGA1UEBxMJR3JvbmluZ2VuMQwwCgYDVQQKEwNUTk8xGDAWBgNVBAsTD0RhdGEgRWNvc3lzdGVtczE0MDIGA1UEAxMrdXJuOnBsYXlncm91bmQ6dHNnOmNvbm5lY3RvcnM6VGVzdENvbm5lY3RvcjAeFw0yMjA4MTYxMjM3MTJaFw0yNDA4MTYxMjM3MTJaMIGTMQswCQYDVQQGEwJOTDESMBAGA1UECBMJR3JvbmluZ2VuMRIwEAYDVQQHEwlHcm9uaW5nZW4xDDAKBgNVBAoTA1ROTzEYMBYGA1UECxMPRGF0YSBFY29zeXN0ZW1zMTQwMgYDVQQDEyt1cm46cGxheWdyb3VuZDp0c2c6Y29ubmVjdG9yczpUZXN0Q29ubmVjdG9yMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAshYSRWa0OSAC3d+4yd8qiBmdkHGSWVo5hYJRYWggY2tBUoZa7iza6Ptzx2BigZS9HLGP9gX6aYpPcLkIjv41NRqPH6BJJ9+mCLruklS0y968cecocl9aNqWBWxmAG3fRzww3j8TsqtE6Hyl45PGt4x6kh9ybB1uiwbTmDdBwpDGpRqEcH089W9n9FXYEB3p9y+27VeF8mvx899VpO6mYlVjUhH/Jfsy0K2n8sVevBpJQFnFUTlo75+4CqlLnKXYubKCrhuXcOFnpHETkVzEV3yOqBDH4BOz8oO2maqJ9x5gbFGJqPOiKVdXnFN9T+SYG+d1bQvmUSeTqq3qUzvtEeQIDAQABo4HDMIHAMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgXgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAdBgNVHQ4EFgQUVQtLTuhr0ByFfw/xpY7QVkxTsCYwHwYDVR0jBBgwFoAUhL4nLKgV89g4smlBd6HpDOxbDNkwRwYDVR0RBEAwPocEfwAAAYIJbG9jYWxob3N0git1cm46cGxheWdyb3VuZDp0c2c6Y29ubmVjdG9yczpUZXN0Q29ubmVjdG9yMA0GCSqGSIb3DQEBCwUAA4IBAQDKMXe6iN+uTcGh2kHd5ed1NzP61sphHp4OcpIamxzrWEQaxyTSZOsQqt1YJTFsdQhV2BsbQS9/VB0j3+IQBr7j5K1DyGm1keXMinblK7WcRkP4LqVjr1DSYHn3rHyELq3GXM97jJ5e0sIwY1y/wlC747oNcLdNTUH6S6Jtr9jY1SMY/RwYej4kECk7jQ/sCbGqVYaIQS8gOAsZtF58c07wBt+VLsxU8r2/IlbMztBFSTLcs9oysvrGDy1MEoLTvoxWYGXojd9oOrGEpvhg0i08HnnTHIVj3qUMIZ6Xdhmkgyh+zuTizDZPiULqEXrfCkxlTH0GcZgXVuTGqqy/k182"
  },
  "ids:title": [
    {
      "@value": "Playground Test Connector Container",
      "@type": "http://www.w3.org/2001/XMLSchema#string"
    }
  ],
  "ids:hasEndpoint": [
    {
      "@type": "ids:ConnectorEndpoint",
      "@id": "https://w3id.org/idsa/autogen/connectorEndpoint/c5e0ad42-aa23-46f2-b1cb-e2a48b856581",
      "ids:accessURL": {
        "@id": "https://test-connector.playground.dataspac.es/router"
      }
    }
  ],
  "ids:hasDefaultEndpoint": {
    "@type": "ids:ConnectorEndpoint",
    "@id": "https://w3id.org/idsa/autogen/connectorEndpoint/c5e0ad42-aa23-46f2-b1cb-e2a48b856581",
    "ids:accessURL": {
      "@id": "https://test-connector.playground.dataspac.es/router"
    }
  },
  "ids:resourceCatalog": [
    {
      "@type": "ids:ResourceCatalog",
      "@id": "urn:playground:tsg:connectors:TestConnector:resources",
      "ids:offeredResource": [
        {
          "@type": "ids:DataResource",
          "@id": "urn:playground:tsg:connectors:TestConnector:resources:f41fe138-d6ab-445a-8624-93d94d4e603b",
          "ids:language": [
            {
              "@id": "https://w3id.org/idsa/code/EN"
            }
          ],
          "ids:version": "1",
          "ids:created": {
            "@value": "2023-09-10T09:04:09.959Z",
            "@type": "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
          },
          "ids:keyword": [
            {
              "@value": "test",
              "@language": "en"
            },
            {
              "@value": "artifact",
              "@language": "en"
            },
            {
              "@value": "playground",
              "@language": "en"
            }
          ],
          "ids:title": [
            {
              "@value": "TNO Security Gateway Documentation",
              "@language": "en"
            }
          ],
          "ids:standardLicense": {
            "@id": "http://www.apache.org/licenses/LICENSE-2.0"
          },
          "ids:resourceEndpoint": [
            {
              "@type": "ids:ConnectorEndpoint",
              "@id": "https://w3id.org/idsa/autogen/connectorEndpoint/acfd481f-fe26-4682-bdcb-2f460ed8b4d6",
              "ids:path": "/artifacts/urn%3Aplayground%3Atsg%3Aconnectors%3ATestConnector%3Aresources%3Af41fe138-d6ab-445a-8624-93d94d4e603b",
              "ids:accessURL": {
                "@id": "https://test-connector.playground.dataspac.es/router"
              }
            }
          ],
          "ids:publisher": {
            "@id": "urn:playground:tsg:TNO"
          },
          "ids:sovereign": {
            "@id": "urn:playground:tsg:TNO"
          },
          "ids:representation": [
            {
              "@type": "ids:Representation",
              "@id": "https://w3id.org/idsa/autogen/representation/312ddbb9-d3ca-445e-9f85-2444d98a232e",
              "ids:instance": [
                {
                  "@type": "ids:Artifact",
                  "@id": "urn:playground:tsg:connectors:TestConnector:artifacts:4510e3df-df71-4c95-8607-b00f64a11ecd",
                  "ids:fileName": "TNO Security Gateway Documentation.pdf",
                  "ids:byteSize": 67466
                }
              ],
              "ids:mediaType": {
                "@type": "ids:IANAMediaType",
                "@id": "https://w3id.org/idsa/autogen/iANAMediaType/2fe3efda-c6d7-4a44-a81e-19046cfe0f61",
                "ids:filenameExtension": "pdf"
              }
            }
          ],
          "ids:description": [
            {
              "@value": "Test artifact Playground",
              "@language": "en"
            }
          ],
          "ids:contractOffer": [
            {
              "@type": "ids:ContractOffer",
              "@id": "https://w3id.org/idsa/autogen/contractOffer/21143045-8e08-4b11-bf8f-4eabb6d1e875",
              "ids:permission": [
                {
                  "@type": "ids:Permission",
                  "@id": "https://w3id.org/idsa/autogen/permission/86903303-0e66-42b2-a37b-b692d32a2f8a",
                  "ids:target": {
                    "@id": "urn:playground:tsg:connectors:TestConnector:artifacts:4510e3df-df71-4c95-8607-b00f64a11ecd"
                  },
                  "ids:action": [
                    {
                      "@id": "https://w3id.org/idsa/code/READ"
                    },
                    {
                      "@id": "https://w3id.org/idsa/code/USE"
                    }
                  ]
                }
              ],
              "ids:contractStart": {
                "@value": "2023-01-01T00:00:00.000Z",
                "@type": "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
              },
              "ids:contractEnd": {
                "@value": "2023-12-31T00:00:00.000Z",
                "@type": "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
              }
            }
          ]
        }
      ]
    },
    {
      "@type": "ids:ResourceCatalog",
      "@id": "urn:playground:tsg:connectors:TestConnector:data-app",
      "ids:offeredResource": [
        {
          "@type": "ids:Resource",
          "@id": "https://w3id.org/idsa/autogen/resource/0de83be4-c092-49c9-90aa-181f46181314",
          "ids:title": [
            {
              "@value": "Test Consumer Agent A",
              "@language": "en"
            }
          ],
          "ids:resourceEndpoint": [
            {
              "@type": "ids:ConnectorEndpoint",
              "@id": "https://w3id.org/idsa/autogen/connectorEndpoint/1031cfb3-a487-41d8-94c0-ca18ba64d649",
              "ids:path": "/urn:playground:tsg:connectors:TestConsumer:AgentA/0.9.2",
              "ids:endpointDocumentation": [
                {
                  "@id": "https://app.swaggerhub.com/apiproxy/registry/I814/httpbin/0.9.2"
                }
              ],
              "ids:accessURL": {
                "@id": "https://test-connector.playground.dataspac.es/router"
              }
            }
          ],
          "ids:sovereign": {
            "@id": "urn:playground:tsg:connectors:TestConsumer:AgentA"
          }
        }
      ]
    }
  ],
  "ids:description": [
    {
      "@value": "Playground Test Connector Container",
      "@type": "http://www.w3.org/2001/XMLSchema#string"
    }
  ],
  "ids:securityProfile": {
    "@id": "https://w3id.org/idsa/code/TRUST_SECURITY_PROFILE"
  },
  "ids:maintainer": {
    "@id": "urn:playground:tsg:TNO"
  },
  "ids:curator": {
    "@id": "urn:playground:tsg:TNO"
  },
  "ids:inboundModelVersion": [
    "4.0.0",
    "4.1.0",
    "4.1.2",
    "4.2.0",
    "4.2.1",
    "4.2.2",
    "4.2.3",
    "4.2.4",
    "4.2.5",
    "4.2.6",
    "4.2.7"
  ],
  "ids:outboundModelVersion": "4.2.7"
}
~~~
</div>
</details>

The TSG Core Container provides an entry point for receiving `DescriptionRequestMessage`s that indicate a request for information about this connector. This can be either the full self description of the connector, but it can also request a specific element of the self description by means of the `requestedElement` property in the `DescriptionRequestMessage`.

The message flow shows both the usage of the user interface of the Core Container as well as the usage of a Data App to request the information. The first scenario is used to request information as a user to browse through the self description to see whether the connector has relevant information to offer. The latter scenario is intended to be used in an automated fashion, where the Data App requests additional information that is required to properly format a "real" request to the other connector.

<div class="mermaid">
sequenceDiagram
    actor User
    participant DA as Data App
    participant TSGC as Consumer TSG Core Container
    participant TSGP as Provider TSG Core Container
    activate TSGC
    alt User Interface
    User->>TSGC: /api/description<br />{connectorId}, {accessUrl},<br />{requestedElement?}, {Accept? | Header}
    TSGC->>TSGC: Construct DescriptionRequestMessage
    else Data App
    DA->>TSGC: DescriptionRequestMessage
    end
    activate TSGP
    TSGC->>TSGP: DescriptionRequestMessage
    TSGP->>TSGP: Process Request
    TSGP-->>TSGC: DescriptionResponseMessage<br />{RequestedElement | JSON-LD}
    alt User Interface
    TSGC-->>User: RequestedElement
    else Data App
    TSGC-->>DA: DescriptionResponseMessage<br />{RequestedElement | JSON-LD}
    end
    deactivate TSGP 
    deactivate TSGC
</div>

<details>
<summary>Example request 1</summary>
<div markdown="1">

~~~ http
GET /api/description?accessUrl=https%3A%2F%2Fprovider-connector%2F&requestedElement=https%3A%2F%2Fprovider-connector%2FelementId HTTP/1.1
Host: core-container
~~~
</div>
</details>
<details>
<summary>Example request 3 & 4</summary>
<div markdown="1">

~~~ http
POST /router HTTP/1.1
Host: core-container
Content-Type: multipart/form-data; boundary=HQBwYMxhGRmvg4WWX5rz7sZmOKPNzk; charset=UTF-8

--HQBwYMxhGRmvg4WWX5rz7sZmOKPNzk
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:DescriptionRequestMessage",
  "@id" : "https://w3id.org/idsa/autogen/descriptionRequestMessage/b4c18872-eba8-4857-8230-a931cdf5363e",
  "ids:modelVersion" : "4.2.7",
  "ids:issuerConnector" : {
    "@id" : "https://consumer-connector"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://provider-connector"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:consumer"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:provider"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/00443c59-57c8-48d3-b21b-036c41c4ee98",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  },
  "ids:issued" : {
    "@value" : "2022-07-07T15:53:03.936+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  }
}
--HQBwYMxhGRmvg4WWX5rz7sZmOKPNzk--
~~~
</div>
</details>
<details>
<summary>Example response 6 & 8</summary>
<div markdown="1">

~~~ http
HTTP/1.1 200
Content-Type: multipart/form-data; boundary=1EAXbWyYH00JOKss59J4OvUf03vJGW; charset=UTF-8

--1EAXbWyYH00JOKss59J4OvUf03vJGW
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:DescriptionResponseMessage",
  "@id" : "https://w3id.org/idsa/autogen/descriptionResponseMessage/ada05089-07d9-4ba4-a45f-f517807bd071",
  "ids:modelVersion" : "4.2.7",
  "ids:correlationMessage" : {
    "@id" : "https://w3id.org/idsa/autogen/descriptionRequestMessage/b4c18872-eba8-4857-8230-a931cdf5363e"
  },
  "ids:issuerConnector" : {
    "@id" : "https://provider-connector"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://consumer-connector"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:provider"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:consumer"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/32298e3c-a50e-475c-9aa0-0ed8c5ee2cd4",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  },
  "ids:issued" : {
    "@value" : "2022-07-07T15:53:03.969+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  }
}
--1EAXbWyYH00JOKss59J4OvUf03vJGW
Content-Disposition: form-data; name="payload"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  ...
}
--1EAXbWyYH00JOKss59J4OvUf03vJGW--
~~~
</div>
</details>

## Broker

The Metadata Broker interactions are the primarily flows to enable the findability in the network, of both the Connectors themselves but also the resources that these Connectors provide in the network.

### Publish Self-Description

The first Metadata Broker interaction is the publication of a self description at the Broker, to make this information accessible to other components in the network. It consists out of a `ConnectorUpdateMessage` that is sent to the Broker with its self description as payload of the message. The trigger for the Core Container to start this interaction is either a fixed time interval or a change event of the metadata coming from the `ResourceManager`.

<div class="mermaid">
sequenceDiagram
    participant TSG as TSG Core Container
    participant Broker as Broker
    activate TSG
    alt Change Event
    TSG->>TSG: [ResourceManager] Self-Description change
    else Interval
    TSG->>TSG: [interval] Keep-alive Self-Description Publish
    end
    TSG->>TSG: Generate Self-Description
    activate Broker
    TSG->>Broker: ConnectorUpdateMessage
    Broker->>Broker: Persist Self-Description
    Broker-->>TSG: MessageProcessedNotification
    deactivate Broker 
    deactivate TSG
</div>

<details>
<summary>Example request 4</summary>
<div markdown="1">

~~~ http
POST /router HTTP/1.1
Host: core-container
Content-Type: multipart/form-data; boundary=NeKIcXxQY1G9tK7z6FhSp5waWocDGd; charset=UTF-8

--NeKIcXxQY1G9tK7z6FhSp5waWocDGd
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:ConnectorUpdateMessage",
  "@id" : "https://w3id.org/idsa/autogen/connectorUpdateMessage/963567f8-896c-4ea4-80b4-036706dbe825",
  "ids:issued" : {
    "@value" : "2022-07-07T16:00:37.014+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  },
  "ids:affectedConnector" : {
    "@id" : "https://consumer-connector"
  },
  "ids:modelVersion" : "4.2.7",
  "ids:issuerConnector" : {
    "@id" : "https://consumer-connector"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://broker"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:consumer"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:broker"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/c18379bb-4c05-4581-8d71-77d4c648963f",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  }
}
--NeKIcXxQY1G9tK7z6FhSp5waWocDGd
Content-Disposition: form-data; name="payload"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  ...
}
--NeKIcXxQY1G9tK7z6FhSp5waWocDGd--
~~~
</div>
</details>
<details>
<summary>Example response 6</summary>
<div markdown="1">

~~~ http
HTTP/1.1 200
Content-Type: multipart/form-data; boundary=zlfYefecFADaCl0GyrCKrYGB2w8Spz; charset=UTF-8

--zlfYefecFADaCl0GyrCKrYGB2w8Spz
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:MessageProcessedNotificationMessage",
  "@id" : "https://w3id.org/idsa/autogen/messageProcessedNotificationMessage/a4c6d723-ca7f-44db-bab5-2c788de227ef",
  "ids:issued" : {
    "@value" : "2022-07-07T16:00:37.045+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  },
  "ids:modelVersion" : "4.2.7",
  "ids:correlationMessage" : {
    "@id" : "https://w3id.org/idsa/autogen/connectorUpdateMessage/963567f8-896c-4ea4-80b4-036706dbe825"
  },
  "ids:issuerConnector" : {
    "@id" : "https://broker"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://consumer-connector"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:broker"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:consumer"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/5c5ed3bb-4b75-4c50-96fe-79c1811513fd",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  }
}
--zlfYefecFADaCl0GyrCKrYGB2w8Spz--
~~~
</div>
</details>

### Broker Query

The second Metadata Broker interaction is the querying of information at the Broker. It can be done either via the user interface of the core container or via a Data App, similar to the [Self Description](#self-description) flow. The message sent to the Broker is a `QueryMessage` with as payload a [SPARQL](https://www.w3.org/TR/rdf-sparql-query/) query. Since in most cases the intended result of a query is an Information Model object, the response is represented in JSON-LD format that can be parsed by the TSG components.

<div class="mermaid">
sequenceDiagram
    actor User
    participant DA as Data App
    participant TSG as TSG Core Container
    participant Broker as Broker
    activate TSG
    alt User Interface
    User->>TSG: /api/description/query<br />{SPARQL Query}
    TSG->>TSG: Construct QueryMessage
    else Data App
    DA->>TSG: QueryMessage<br />{SPARQL Query}
    end
    activate Broker
    TSG->>Broker: QueryMessage<br />{SPARQL Query}
    Broker->>Broker: Query Backend
    Broker-->>TSG: ResultMessage<br />{SPARQL Result | JSON-LD}
    alt User Interface
    TSG-->>User: SPARQL Result
    else Data App
    TSG-->>DA: ResultMessage<br />{SPARQL Result | JSON-LD}
    end
    deactivate Broker
    deactivate TSG
</div>

<details>
<summary>Example request 1</summary>
<div markdown="1">

~~~ http
POST /api/description/query?accessUrl=https%3A%2F%2Fbroker%2F&queryLanguage=SPARQL&recipientScope=BROKER HTTP/1.1
Host: core-container
Content-Type: application/sparql-query

PREFIX ids: <https://w3id.org/idsa/core/>
DESCRIBE * WHERE {
  GRAPH ?g {
    ?s ?o ?p.
  }
}
~~~
</div>
</details>
<details>
<summary>Example request 3 & 4</summary>
<div markdown="1">

~~~ http
POST /router HTTP/1.1
Host: core-container
Content-Type: multipart/form-data; boundary=dFJ08uTQEUqy3duHN6fjsL9Y568sPt; charset=UTF-8

--dFJ08uTQEUqy3duHN6fjsL9Y568sPt
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:QueryMessage",
  "@id" : "https://w3id.org/idsa/autogen/queryMessage/2c563490-eb54-46a4-9d07-d083046a0f15",
  "ids:queryLanguage" : {
    "@id" : "https://w3id.org/idsa/code/SPARQL"
  },
  "ids:queryScope" : {
    "@id" : "https://w3id.org/idsa/code/ALL"
  },
  "ids:recipientScope" : {
    "@id" : "https://w3id.org/idsa/code/BROKER"
  },
  "ids:modelVersion" : "4.2.7",
  "ids:issuerConnector" : {
    "@id" : "https://consumer-connector"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://broker"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:consumer"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:broker"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/894c1405-1c1f-47ac-af0f-4a9be21e013f",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  },
  "ids:issued" : {
    "@value" : "2022-07-07T16:22:11.986+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  }
}
--dFJ08uTQEUqy3duHN6fjsL9Y568sPt
Content-Disposition: form-data; name="payload"
Content-Type: application/sparql-query

PREFIX ids: <https://w3id.org/idsa/core/>
DESCRIBE * WHERE {
  GRAPH ?g {
    ?s ?o ?p.
  }
}
--dFJ08uTQEUqy3duHN6fjsL9Y568sPt--
~~~
</div>
</details>
<details>
<summary>Example response 6 & 8</summary>
<div markdown="1">

~~~ http
HTTP/1.1 200
Content-Type: multipart/form-data; boundary=KkrF2zP6NLtv5UKemeKgZZnS7PuWgd; charset=UTF-8

--KkrF2zP6NLtv5UKemeKgZZnS7PuWgd
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:ResultMessage",
  "@id" : "https://w3id.org/idsa/autogen/resultMessage/6318a675-3b31-4fe0-bc85-d99f33023a15",
  "ids:modelVersion" : "4.2.7",
  "ids:correlationMessage" : {
    "@id" : "https://w3id.org/idsa/autogen/queryMessage/2c563490-eb54-46a4-9d07-d083046a0f15"
  },
  "ids:issuerConnector" : {
    "@id" : "https://broker"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://consumer-connector"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:broker"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:consumer"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/b1cde727-cad0-423b-b7a8-dd18a1ca8319",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  },
  "ids:issued" : {
    "@value" : "2022-07-07T16:22:12.022+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  }
}
--KkrF2zP6NLtv5UKemeKgZZnS7PuWgd
Content-Disposition: form-data; name="payload"
Content-Type: application/sparql-query

{
    ...
}
--KkrF2zP6NLtv5UKemeKgZZnS7PuWgd--
~~~
</div>
</details>

## Artifact Request

The Artifact request flow is the most simplistic way of exchanging information between two connectors, by means of artifacts. The request follows the `ids:ArtifactRequestMessage` mentioning the identifier of the intended artifact (`requestedArtifact`) and with a response in the form of an `ids:ArtifactResponseMessage`. The TSG components assume a Base64 encoded string as the payload of such an `ids:ArtifactResponseMessage` to be able to handle binary files to be exchanged.

<div class="mermaid">
sequenceDiagram
    actor User
    participant TSGC as Consumer TSG Core Container
    participant TSGP as Provider TSG Core Container

    activate User
    User->>TSGC: /api/artifacts/consumer/artifact<br />{artifact}, {connectorId},<br />{accessUrl}, {transferContract}
    activate TSGC
    TSGC->>TSGP: ArtifactRequestMessage<br />{requestedArtifact}, {transferContract}
    activate TSGP
    TSGP->>TSGP: Verify access
    TSGP->>TSGP: Retrieve Artifact
    TSGP-->>TSGC: ArtifactResponseMessage<br />{artifact | b64enc}
    deactivate TSGP
    TSGC-->>TSGC: Extract artifact
    TSGC-->>User: Artifact
    deactivate TSGC
    deactivate User
</div>

<details>
<summary>Example request 1</summary>
<div markdown="1">

~~~ http
POST /api/description/query?accessUrl=https%3A%2F%2Fbroker%2F&queryLanguage=SPARQL&recipientScope=BROKER HTTP/1.1
Host: core-container
Content-Type: multipart/form-data; boundary=xqkzeCgvF6tZgiVjatJKMlOpN50k0G; charset=UTF-8

--xqkzeCgvF6tZgiVjatJKMlOpN50k0G
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:ArtifactRequestMessage",
  "@id" : "https://w3id.org/idsa/autogen/artifactRequestMessage/9b66f4dd-d982-4017-beb2-2dab570485af",
  "ids:requestedArtifact" : {
    "@id" : "urn:ids:provider:artifacts:ArtifactID"
  },
  "ids:modelVersion" : "4.2.7",
  "ids:issuerConnector" : {
    "@id" : "https://consumer-connector"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://broker"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:consumer"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:provider"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/cba62346-a2c4-49dd-afa5-7a1a30fbe24e",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  },
  "ids:transferContract" : {
    "@id" : "urn:ids:provider:contracts:ContractID"
  },
  "ids:issued" : {
    "@value" : "2022-07-15T12:23:12.881+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  }
}
--xqkzeCgvF6tZgiVjatJKMlOpN50k0G--
~~~
</div>
</details>
<details>
<summary>Example request 2</summary>
<div markdown="1">

~~~ http
POST /api/description/query?accessUrl=https%3A%2F%2Fbroker%2F&queryLanguage=SPARQL&recipientScope=BROKER HTTP/1.1
Host: core-container
Content-Type: multipart/form-data; boundary=e7HZlmqYvz6mOTuQCyvKAgBN3NyeCY; charset=UTF-8

--e7HZlmqYvz6mOTuQCyvKAgBN3NyeCY
Content-Disposition: form-data; name="header"
Content-Type: application/ld+json

{
  "@context" : {
    "ids" : "https://w3id.org/idsa/core/",
    "idsc" : "https://w3id.org/idsa/code/"
  },
  "@type" : "ids:ArtifactResponseMessage",
  "@id" : "https://w3id.org/idsa/autogen/artifactResponseMessage/0b489b68-24ef-4d33-8fcf-a498fa09b6ec",
  "ids:modelVersion" : "4.2.7",
  "ids:correlationMessage" : {
    "@id" : "https://w3id.org/idsa/autogen/artifactRequestMessage/9b66f4dd-d982-4017-beb2-2dab570485af"
  },
  "ids:issuerConnector" : {
    "@id" : "https://broker"
  },
  "ids:recipientConnector" : [ {
    "@id" : "https://consumer-connector"
  } ],
  "ids:senderAgent" : {
    "@id" : "urn:ids:provider"
  },
  "ids:recipientAgent" : [ {
    "@id" : "urn:ids:consumer"
  } ],
  "ids:securityToken" : {
    "@type" : "ids:DynamicAttributeToken",
    "@id" : "https://w3id.org/idsa/autogen/dynamicAttributeToken/e9795163-1bcc-419d-b4f7-624caf73cf8b",
    "ids:tokenValue" : "DUMMY",
    "ids:tokenFormat" : {
      "@id" : "https://w3id.org/idsa/code/JWT"
    }
  },
  "ids:transferContract" : {
    "@id" : "urn:ids:provider:contracts:ContractID"
  },
  "ids:issued" : {
    "@value" : "2022-07-15T12:23:12.918+02:00",
    "@type" : "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
  }
}
--e7HZlmqYvz6mOTuQCyvKAgBN3NyeCY
Content-Disposition: form-data; name="payload"
Content-Transfer-Encoding: base64

RmlsZSBjb250ZW50cw==
--e7HZlmqYvz6mOTuQCyvKAgBN3NyeCY--
~~~
</div>
</details>
<details>
<summary>Example request 1</summary>
<div markdown="1">

~~~ http
POST /api/description/query?accessUrl=https%3A%2F%2Fbroker%2F&queryLanguage=SPARQL&recipientScope=BROKER HTTP/1.1
Host: core-container
Content-Type: application/sparql-query

PREFIX ids: <https://w3id.org/idsa/core/>
DESCRIBE * WHERE {
  GRAPH ?g {
    ?s ?o ?p.
  }
}
~~~
</div>
</details>

## Data App interaction

The Data App interaction is the most generic and versatile message flow, since the handling of the messages is done inside the Data Apps. The Core Container acts primarily as gateway for the message, while still checking the messages for Identification, Authentication, and Authorization purposes. The Data Apps can build in support for any of the IDS Messages described in [IDS Messages]({{ '/docs/communication/ids-messages' | relative_url }}) documentation.

Examples of Data Apps can be found in the [Existing Apps]({{ '/docs/data-apps/existing' | relative_url }}) section.

<div class="mermaid">
sequenceDiagram
    participant DAC as Consumer Data App
    participant TSGC as Consumer TSG Core Container
    participant TSGP as Provider TSG Core Container
    participant DAP as Provider Data App

    activate DAC
    activate TSGC
    DAC->>TSGC: /https_out*<br />{IDS Message | JSON-LD}, {Payload}
    activate TSGP
    TSGC->>TSGP: /router*<br />{IDS Message | JSON-LD}, {Payload}
    activate DAP
    TSGP->>DAP: /router*<br />{IDS Message | JSON-LD}, {Payload}
    DAP->>DAP: Process Message
    DAP-->>TSGP: {IDS Message | JSON-LD}, {Payload}
    deactivate DAP
    TSGP-->>TSGC: {IDS Message | JSON-LD}, {Payload}
    deactivate TSGP
    TSGC-->>DAC: {IDS Message | JSON-LD}, {Payload}
    deactivate TSGC
    DAC-->>DAC: Process Response
    deactivate DAC
</div>

## Policy Negotiation

The Policy Negotiation interaction shows the details of the process described in the [Policy Enforcement Framework]({{ '/docs/core-container/pef#negotiation' | relative_url }}) section.

In the message flow, a lot of alternatives are modeled. The reason for this is to show not only the happy flow scenarios, but also alternative scenarios where more information is required or where the contract is rejected.

<div class="mermaid">
sequenceDiagram
    actor User
    participant DA as Data App
    participant TSGC as Consumer TSG Core Container
    participant TSGP as Provider TSG Core Container
    alt User Interface
    activate User
    activate TSGC
    User->>TSGC: /api/artifacts/consumer/contractRequest<br />{connectorId}, {contractOffer | JSON-LD}, {accessUrl}
    TSGC->>TSGC: Construct ContractRequestMessage
    else Data App
    activate DA
    DA->>TSGC: ContractRequestMessage<br />{ContractRequest | JSON-LD}
    end
    activate TSGP
    TSGC->>TSGP: ContractRequestMessage<br />{ContractRequest | JSON-LD}
    TSGP->>TSGP: Evaluate ContractRequest
    alt Contract Accepted
    TSGP-->>TSGC: ContractAgreementMessage<br />{ContractAgreement | JSON-LD}
    TSGC->>TSGP: ContractAgreementMessage<br />{ContractAgreement | JSON-LD}
    TSGP-->>TSGC: MessageProcessedNotification
    alt User Interface
    deactivate TSGP
    TSGC-->>User: ContractAgreement
    else Data App
    TSGC-->>DA: ContractAgreementMessage<br />{ContractAgreement | JSON-LD}
    end
    else Contract Rejected
    TSGP-->>TSGC: ContractRejectionMessage<br />{ContractRejection | JSON-LD}
    alt User Interface
    TSGC-->>User: ContractRejection
    else Data App
    TSGC-->>DA: ContractRejectionMessage<br />{ContractRejection | JSON-LD}
    end
    end
    deactivate User
    deactivate DA
    deactivate TSGC
</div>

## DAPS Token Request

The DAPS Token Request is the simplest interaction, but arguably one of the most important in the workings of a dataspace, as with this request a Dynamic Attribute Token (DAT) is requested that provides the trust and information needed for the Identification and Authentication processes. The DAT received from the Dynamic Attribute Provisioning Service (DAPS) is a verifiable claim of the DAPS stating that the claimed identity matches the requester accompanied with the dynamic attributes the DAPS knows of the identity.

<div class="mermaid">
sequenceDiagram
    participant TSG as TSG Core Container
    participant DAPS as Dynamic Attribute Provisioning Service

    activate TSG
    TSG->>TSG: Create DAT Request Payload<br />{DatRequestPayload | JSON-LD}
    TSG->>TSG: Construct signed JWT<br />{client_assertion}
    activate DAPS
    TSG->>DAPS: /token<br />{grant_type}, {client_assertion_type}, {client_assertion}, {scope}
    DAPS->>DAPS: Verify identity and fetch dynamic attributes
    DAPS-->>TSG: Token<br />{access_token}, {token_type}
    deactivate TSG
    deactivate DAPS
</div>
