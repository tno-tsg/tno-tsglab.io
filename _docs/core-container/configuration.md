---
layout: doc
title: Configuration
toc: true
left_menu: true
categories: core-container
slug: configuration
---

The configuration of the Core Container follows [Spring Configuration Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config.typesafe-configuration-properties) to setup all of the initial parameters. The Core Container project contains type-safe properties that can be autowired into components. There is only one way to configure the Core Container, meaning there is only one mode of operation.

Not just the specific Core Container properties can be configured in this way, but also more generic Spring or Camel configuration can be provided. For instance, when you'd want to alter the logging settings.

## Overview

The configuration properties are usually set by providing an `application.yaml` in the current working directory of the Java process, when using the Helm charts for deployment the locations of the configuration are pre-configured and everything under the `ids` key will be placed in the `application.yaml` file. Apart from setting the configuration properties by providing a file, Spring also allows you to provide environment variables to set configuration properties. For instance, the environment variable `KEYSTORE_PEM_CERT` will overwrite the `keystore.pem.cert` property. Environment variables will override properties set in the `application.yaml` configuration.

Spring uses [relaxed binding](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config.typesafe-configuration-properties.relaxed-binding) to allow for non-exact matches, the most used relaxed rules are: kebab case vs. camel case, case-insensitive, dot vs. underscore delimiters. So all of the following notations will match the same property: `broker.autoRegister`, `broker.auto-register`, `BROKER_AUTOREGISTER`, `broker.auto_register`.

## Spring Configuration properties

The table below shows all of the configuration properties that are defined by the Core Container. At the bottom of this page a minimal example is shown of the YAML structure following this.

| Configuration Key | Type (* required) | Default | Description |
|---|---|---|---|---|
| **artifacts** |
| &nbsp;&nbsp;&nbsp;.enabled | Boolean | `true` | Whether automatic artifact handling should be enabled |
| &nbsp;&nbsp;&nbsp;.location | String | `/resources` | File location where artifacts will be stored |
| &nbsp;&nbsp;&nbsp;.dapsValidation | Boolean | `true` | Whether DAPS should be enabled for artifacts |
| &nbsp;&nbsp;&nbsp;.policyEnforcement | Boolean | `true` | Whether policy enforcement should be enabled for artifacts |
| &nbsp;&nbsp;&nbsp;.encryptAtRest | Boolean | `true` | Whether encryption at rest should be anabled for artifacts |
| &nbsp;&nbsp;&nbsp;.aesEncryptionKey | String | - | AES encryption key for encryption at rest in Base64 encoding |
| **broker** |
| &nbsp;&nbsp;&nbsp;.id | URI* | - | Broker IDS Identifier |
| &nbsp;&nbsp;&nbsp;.address | URL* | - | Broker Access URL |
| &nbsp;&nbsp;&nbsp;.autoRegister | Boolean | `true` | Automatic registration at the Broker |
| &nbsp;&nbsp;&nbsp;.profile | `MINIMAL`, `FULL` | `MINIMAL` | Self-Description Profile to share with the Broker |
| &nbsp;&nbsp;&nbsp;.reRegisterInterval | Float | `1` | Interval (in hours) for automatic re-registration |
| &nbsp;&nbsp;&nbsp;.brokerInitialDelay | Long | `10000` | Initial delay (in milliseconds) for initial Broker registration |
| &nbsp;&nbsp;&nbsp;.registrationMaxRetries | Int | `30` | Maximum of retries for initial Broker registration |
| &nbsp;&nbsp;&nbsp;.registrationBackoffPeriod | Long | `10000` | Backoff period (in milliseconds) for initial Broker registration |
| &nbsp;&nbsp;&nbsp;.brokerHealthCheckInterval | Long | `3600000` | Broker health check interval (in milliseconds) |
| **info** |
| &nbsp;&nbsp;&nbsp;.idsid | URI* | - | Connector IDS identifier |
| &nbsp;&nbsp;&nbsp;.titles | String[]* | - | Connector titles |
| &nbsp;&nbsp;&nbsp;.descriptions | String[]* | - | Connector descriptions |
| &nbsp;&nbsp;&nbsp;.accessUrl | URL[]* | - | Connector Access URL for external connectors |
| &nbsp;&nbsp;&nbsp;.curator | URI* | - | Curator of the contents of this connector, an IDS participant identifier |
| &nbsp;&nbsp;&nbsp;.maintainer | URI* | - | Technical administrator of this connector, an IDS participant identifier |
| &nbsp;&nbsp;&nbsp;.securityProfile | `TRUST_SECURITY_PROFILE` | - | Security Profile as set as self-declaration in the metadata of this connector |
| **daps** |
| &nbsp;&nbsp;&nbsp;.url | String* | - | URL of the DAPS |
| &nbsp;&nbsp;&nbsp;.issuerUrl | String | - | Issuer URL of the DAPS, only required if DAPS URL structure is different |
| &nbsp;&nbsp;&nbsp;.tokenUrl | String | - | Token URL of the DAPS, only required if DAPS URL structure is different |
| &nbsp;&nbsp;&nbsp;.jwksUrl | String | - | JWKS URL of the DAPS, only required if DAPS URL structure is different |
| &nbsp;&nbsp;&nbsp;**.additionalDaps[]** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.url | String* | - | URL of the additional DAPS |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.issuerUrl | String | - | Issuer URL of the additional DAPS, only required if DAPS URL structure is different |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.jwksUrl | String | - | JWKS URL of the additional DAPS, only required if DAPS URL structure is different |
| &nbsp;&nbsp;&nbsp;.requiredSecurityProfile | `BASE`, `TRUST`, `TRUST_PLUS` | `BASE` | Minimal required security profile |
| &nbsp;&nbsp;&nbsp;**.dynamicClaims** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**.transportCertsSha256** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.certLocation | String | - | Location of certificate used for transport |
| httpMode | `MULTIPART_FORM_DATA`, `MULTIPART_MIXED`, `REST` | `MULTIPART_MIXED` | IDS HTTP mode |
| **keystore** |
| &nbsp;&nbsp;&nbsp;.type | `PEM` | `PEM` | Type of supplied keystore |
| &nbsp;&nbsp;&nbsp;.pem.cert | String* | - | IDS Certificate PEM-based certificate, either starting with `file:` to point to a file resource or direct certificate (plain or Base64 encoded) |
| &nbsp;&nbsp;&nbsp;.pem.key | String* | - | IDS Certificate PKCS#8-based key, either starting with `file:` to point to a file resource or direct key (plain or Base64 encoded) |
| **resourceDatabase** |
| &nbsp;&nbsp;&nbsp;.hostname | String* | - | MongoDB hostname |
| &nbsp;&nbsp;&nbsp;.port | Integer | `27017` | MongoDB port |
| &nbsp;&nbsp;&nbsp;.username | String | - | MongoDB username |
| &nbsp;&nbsp;&nbsp;.password | String | - | MongoDB password |
| &nbsp;&nbsp;&nbsp;.authenticationDatabase | String | - | Authentication Database that contains the user to use |
| &nbsp;&nbsp;&nbsp;.database | String* | - | MongoDB database containing the relevant collection |
| &nbsp;&nbsp;&nbsp;.collection | String | - | MongoDB collection |
| &nbsp;&nbsp;&nbsp;.isSslEnabled | Boolean | `false` | Connect to MongoDB via SSL |
| &nbsp;&nbsp;&nbsp;.isWatchable | Boolean | `false` | Configuration on whether the MongoDB is watchable, i.e. is a replica set |
| namespaces | Map<String, String> | - | Additionally supported namespaces in IDS Infomodel classes (in the properties field) |
| **remoteAttestation** |
| &nbsp;&nbsp;&nbsp;**.tpm** |
| &nbsp;&nbsp;&nbsp;.simulator | Boolean | `false` | Deploy a simulator instead of using a physical TPM |
| &nbsp;&nbsp;&nbsp;.host | String | - | TPM Server host |
| &nbsp;&nbsp;&nbsp;.port | Integer | - | TPM Server port |
| &nbsp;&nbsp;&nbsp;**.ttp** |
| &nbsp;&nbsp;&nbsp;.hostname | String* | - | Hostname of the Trusted Third Party |
| &nbsp;&nbsp;&nbsp;.port | Integer | `443` | Port of the Trusted Third Party |
| **routes** |
| &nbsp;&nbsp;&nbsp;.https | Boolean | `false` | Enable TLS for Camel routes, based on IDS certificate |
| &nbsp;&nbsp;&nbsp;**.ingress** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**.http[]** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.id | String | - | Identifier of the route (locally unique) |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.preProcessing | String[] | - | Pre processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.postProcessing | String[] | - | Post processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.port | Integer | `8080` | Exposed port for incoming traffic |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.endpoint | String | `` | Endpoint prefix for incoming traffic |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.parameters | String | `` | Additional Camel HTTP parameters |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.dataApp | String* | - | Data App Endpoint |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.clearing | Boolean | `false` | Clearing House flag for automatic clearing of message |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.dapsVerify | Boolean | `true` | DAPS Verification flag for automatic verification of incoming requests |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.policyEnforcement | Boolean | `false` | Policy Enforcement flag |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.delegatedPolicyNegotiation | Boolean | `false` | Delegate policy negotiation to the data app |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**.idscp[]** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.id | String | - | Identifier of the route (locally unique) |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.preProcessing | String[] | - | Pre processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.postProcessing | String[] | - | Post processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.port | Integer | `9292` | Exposed port for incoming traffic |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.dataApp | String* | - | Data App Endpoint |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.clearing | Boolean | `false` | Clearing House flag for automatic clearing of message |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.tlsClientHostnameVerification | Boolean | `true` | Flag for enabling hostname verification on TLS level |
| &nbsp;&nbsp;&nbsp;**.egress** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**.http[]** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.id | String | - | Identifier of the route (locally unique) |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.preProcessing | String[] | - | Pre processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.postProcessing | String[] | - | Post processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.listenPort | Integer | `8080` | Exposed port for internal traffic |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.endpoint | String | `https_out` | Endpoint prefix for internal traffic |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.allowHTTP | Boolean | `true` | Allow plain HTTP requests |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.clearing | Boolean | `false` | Clearing House flag for automatic clearing of message |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.dapsInject | Boolean | `true` | DAPS Injection flag for automatic injection of Dynamic Attribute Tokens to outgoing requests |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.forwardHeader | String | `Forward-To` | Header used for indicating the intended recipient of the request |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**.idscp[]** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.id | String | - | Identifier of the route (locally unique) |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.preProcessing | String[] | - | Pre processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.postProcessing | String[] | - | Post processing steps to be executed |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.listenPort | Integer | `8080` | Exposed port for internal traffic |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.endpoint | String | `idscp_out` | Endpoint prefix for internal traffic |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.clearing | Boolean | `false` | Clearing House flag for automatic clearing of message |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.dapsInject | Boolean | `true` | DAPS Injection flag for automatic injection of Dynamic Attribute Tokens to outgoing requests |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.forwardHeader | String | `Forward-To` | Header used for indicating the intended recipient of the request |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.restOverIDSCP | Boolean | `false` | Flag that indicates that REST over IDSCP is used |
| **truststore** |
| &nbsp;&nbsp;&nbsp;.type | PEM, SYSTEM, ACCEPT_ALL | `PEM` | Type of supplied truststore |
| &nbsp;&nbsp;&nbsp;.pem |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.chain | String* | - | Concatenated PEM CA certificates to be loaded in the Truststore, either starting with `file:` to point to a file resource or direct certificate chain (plain or Base64 encoded) |
| **orchestrationManagerConfig** |
| &nbsp;&nbsp;&nbsp;.enableKubernetes | Boolean | `false` | Enable Kubernetes Orchestration Manager |
| &nbsp;&nbsp;&nbsp;.masterUrl | String | - | Kubernetes cluster master URL |
| &nbsp;&nbsp;&nbsp;.clientCertificate | String | - | Client certificate file |
| &nbsp;&nbsp;&nbsp;.clientKey | String | - | Client key file |
| &nbsp;&nbsp;&nbsp;.certificateAuthorityData | String | - | Certificate Authority data |
| &nbsp;&nbsp;&nbsp;.pullSecretName | String | - | Global pull-secret, used when no explicit pull-secret is given for a container |
| &nbsp;&nbsp;&nbsp;.pullPolicy | String | `IfNotPresent` | Kubernetes Image Pull Policy |
| &nbsp;&nbsp;&nbsp;.namespace | String | - | Kubernetes Namespace to use for resources, defaults to the current namespace (if in a Pod) or to "default" |
| &nbsp;&nbsp;&nbsp;.timeout | Long | `900` | Timeout (in seconds) for Kubernetes Completable Futures |
| **pef** |
| &nbsp;&nbsp;&nbsp;**.negotiation** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.enabled | Boolean | `true` | Enable automatic Policy Negotiation |
| &nbsp;&nbsp;&nbsp;**.pdp** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.enabled | Boolean | `true` | Boolean on whether Policy Decision is enabled |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.defaultPolicy | `DENY_UNLESS`, `ALLOW_UNLESS` | `DENY_UNLESS` | Default Policy in case no matching policy can be found |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.allowEmptyContract | Boolean | `false` | Allow incoming messages without transfer contract |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.allowUnmappableMessage | Boolean | `false` | Allow incoming message that are not supported for automatic mapping to a Policy Decision Request context |
| **security** |
| &nbsp;&nbsp;&nbsp;.enabled | Boolean | `false` | Enable Spring Security |
| &nbsp;&nbsp;&nbsp;**.users[]** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.id | String* | - | User ID |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.password | String* | - | Password in BCrypt format |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.roles | String[] | - | User Role list |
| &nbsp;&nbsp;&nbsp;**.apiKeys[]** |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.id | String* | - | API key ID |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.key | String* | - | API key |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.roles | String[] | - | User Role list |
| &nbsp;&nbsp;&nbsp;.maxFailedAttempts | Integer | `10` | Maximum of failed login attempts before temporarily locking account |
| &nbsp;&nbsp;&nbsp;.failedAttemptDelay | Long | `900000` | Time window in which the maximum of `maxFailedAttempts` is allowed in ms |
| &nbsp;&nbsp;&nbsp;.accountLockDuration | Long | `900000` | Time period an account is locked in ms |
| &nbsp;&nbsp;&nbsp;.httpsOnly | Boolean | `false` | Enable runtime check that stops the container if one of its endpoints is reachable via non encrypted HTTP |
| &nbsp;&nbsp;&nbsp;.tlsVersions | String[] | `['TLSv1.3']` | List of accepted TLS protocols for both ingress and egress |
| &nbsp;&nbsp;&nbsp;.cipherSuites | String[] | `[]` | List of Cipher suites, empty list indicates default Apache ciphers |
| &nbsp;&nbsp;&nbsp;.passwordRotation | Boolean | `false` | Whether you want to enable password rotation. |
| &nbsp;&nbsp;&nbsp;.rotationPeriod | Long | `90*24*60*60*1000L` | The period of password rotation. I.e. after how many days users should change their password/api keys. Defaults to 90 days. |
| &nbsp;&nbsp;&nbsp;.ntpInMilliseconds | Long | `100` | The maximum amount of milliseconds allowed as a difference between NTP time and system time. Defaults to 100ms. |
| &nbsp;&nbsp;&nbsp;.ntpCheckInterval | Long | `60*1000` | The interval in which the NTP time is checked with the system time. Defaults to 60000 milliseconds. |
| &nbsp;&nbsp;&nbsp;.ntpLogging | Boolean | `false` | Boolean for whether you want to display NTP logs or not. Defaults to false. |
| **selfdescription** |
| &nbsp;&nbsp;&nbsp;.localEndpoint | Boolean | `true` | Enable local multipart description endpoint |
| &nbsp;&nbsp;&nbsp;.localPlainEndpoint | Boolean | `true` | Enable local plain description endpoint |
| **workflow** |
| &nbsp;&nbsp;&nbsp;.incrementalIds | Boolean | `false` | Use incremental identifiers rather than random UUIDs |
| &nbsp;&nbsp;&nbsp;.useOrchestration | Boolean | `false` | Boolean that indicates whether or not Data App Orchestration should be used by the workflow manager |
| &nbsp;&nbsp;&nbsp;.internalHostname | String | - | The internal hostname of this core container that can be reached by Data Apps |
| &nbsp;&nbsp;&nbsp;.saveIntermediateResults | Boolean | `false` | Debug flag for following intermediate results |

<center><strong>Configuration properties</strong></center>

## Security evaluation
On the healthcheck url `/healthcheck`, a JSON object is returned with an assesment of how secure the deployment is. All booleans should be set to true to have the most secure deployment. The table below lists the JSON property and the corresponding config parameter.

| JSON Parameter | Configuration parameter | Description |
| --- | --- | --- |
| **routeConfiguration** |
| &nbsp;&nbsp;&nbsp;.routesUsingTLS | routeConfig.https | Checks if all routes are using communication via HTTPS. |
| &nbsp;&nbsp;&nbsp;.noCustomRoutes | routeConfig.allRoutes | Checks if no custom routes are used, because these cannot be verified. |
| &nbsp;&nbsp;&nbsp;.camelRoutesViaDapsVerify | routeConfig.ingress.http | Checks if all ingress routes use DAPS verification. |
| &nbsp;&nbsp;&nbsp;.CamelRoutesIDSCPIngress<br />WithHostnameVerification | routeConfig.ingress.idscp | Checks if hostname verification is enabled for the TLS Client. |
| **artifacts** |
| &nbsp;&nbsp;&nbsp;.encryptionAtRest | artifactConfiguration.encryptAtRest | Checks if encryption at rest is enabled. |
| &nbsp;&nbsp;&nbsp;.secureAesKey | artifactConfiguration.aesEncryptionKey | Checks if the default AES encryption key has been changed. |
| &nbsp;&nbsp;&nbsp;.allExternalAccessUrlHttps | connectorInfo.accessUrl | Checks if the protocol of the access urls are using HTTPS. |
| &nbsp;&nbsp;&nbsp;.securityEnabled | securityConfig.enabled | Checks if the security configuration is enabled, so users and API keys can be setup. |
| &nbsp;&nbsp;&nbsp;.LimitedTrustStore | trustStoreConfig.type | Checks if the truststore is setup to not accept all certificates. |
| &nbsp;&nbsp;&nbsp;.OCSP | Environment variable `ENABLE_OCSP` (defaults to true) | Checks if OCSP is enabled|
| &nbsp;&nbsp;&nbsp;.APIKeysHaveMinimumOf32Chars | securityConfig.apiKeys | Checks if API keys have a minimal length of 32 characters. |
| &nbsp;&nbsp;&nbsp;.TLSVersion1.3Only | securityConfig.tlsVersions | Checks if the TLS versions that are accepted are TLS 1.3 only. |
| **Optional** |
| policyEnforcementOnCamelRoutes | routeConfig.ingress.http | Checks if the policyEnforcement is enabled for Camel routes. |

On top of the dynamic security evaluation, some checks are executed on startup of the connector. If these checks fail, the connector will terminate. These checks are:
- _NTP Clock skew_: On startup the system clock is evaluated against a server in the [`pool.ntp.org`](https://www.ntppool.org/). If the clock skew is larger than 100 milliseconds, it is most likely the case that the server clock is configured incorrectly. This will give problems during runtime of the connector, especially around setting up TLS connections. Therefore the connector won't start with a clock skew larger then 100 milliseconds (both towards the past as the future). Next to the check on startup, the connector will also check every minute if the clock skew is present. If it is, the connector will shutdown.
- _HTTPS Only_: If in the security config the `httpsOnly` property is set to true, the connector will check on startup whether all communication, both internal as external, is encrypted. If that's not the case, the connector won't start. Configuration of the connector is only possible via editing yaml files and deploying the connector. We assume that everyone who has access to either the docker-compose- or kubernetes environment of the core container also has administrator rights within the core container, because it is inherit to the position that the cluster/vm adminstrator has. When the connector is offline, it is ensured that non adminstrators cannot tamper with the configuration, because it would require access to the environment where the connector is running. Therefore, only administrators can make changes to the configuration of the core container.

Next to the security aspects of the configuration, the connector also validates the given input for configuration and in case fields contain unexpected or unparseable contents (e.g. wrong serializations of key material) and will automatically stop the container in case this happens.

The truststores used throughout the connector are a combination of the default JVM truststore and the truststore configured via `truststore.pem.chain`. Which allows the connector to connect to dataspaces that fully use their proprietary PKI, including for TLS encryption. But also allows a proprietary PKI that only is used for interaction with the DAPS and a public PKI that is used for TLS termination. The latter requires the `transportCertsSha256` to be configured to strictly pin the proprietary certificate to a public certificate. This transport certificate uses SHA256 for its hashing and for signing it should use an RSA key of 2048 bits.

The configured certificates might expire or be revoked during the runtime of the connector. A new certificate signing request might be necessary to be sent to the Certificate Authority. The processes of the Certificate Authority to handle renewal of revoked certificates might differ, so please contact your Certificate Authority to follow the right procedures. The core container accesses the certificates on a read-only basis. Therefore, to apply a new certificate a new instance of the core container should be deployed. In configurations using Kubernetes, this redeployment can be handled through rolling upgrades. For Docker-Compose configurations, scaling the connector to 2 instances and after successful deployment scaling back to 1 instance yields the same result.

## Additional properties

Additional Spring or Camel properties can be provided next to the Core Container properties. This can be used to configure the logging properties of the Core Container.

For example, the following snippet can be added to the `application.yaml` to set the global logging level to `INFO` and the Core Container logging to debug:

~~~ yaml
logging:
  level:
    root: INFO
    nl.tno.ids: DEBUG
~~~

## Example

~~~ yaml
broker:
  address: https://broker
  autoRegister: false
  id: urn:ids:broker

daps:
  url: https://daps.aisec.fraunhofer.de/v2

info:
  accessUrl:
    - http://localhost:8080
  curator: urn:ids:localhost
  descriptions:
    - IDSA Plugfest Localhost Connector
  idsid: http://plugfest2021.08.localhost.demo
  maintainer: urn:ids:localhost
  titles:
    - IDSA Plugfest Localhost Connector

keystore:
  type: PEM
  pem:
    # Truncated certificate and key
    cert: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0t...
    key: LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0t...

orchestrationManagerConfig:
  enableKubernetes: true
  pullSecretName: 'pull-secret'

routes:
  egress:
    http:
      - clearing: false
        dapsInject: true
        id: HttpsOut

truststore:
  type: ACCEPT_ALL

workflow:
  incrementalIds: true
  internalHostname: 'host.docker.internal'
  type: IDS
  useOrchestration: true
~~~
