---
layout: doc
title: Overview
toc: true
left_menu: true
slug: overview
tags: documentation, overview
---

This documentation provides information and insights into the different components of the TNO Security Gateway (TSG). The documentation that you are reading concerns version 1.2.3 of the TSG.

The TSG components allows you to participate in an IDS dataspace to exchange information with other organizations with data sovereignty in mind. You will be able to participate with the provided components as-is, but you're allowed to modify the components to create your own dataspace with specific use cases in mind. The source code is open-source with the Apache 2.0 license, and is available in the [TNO TSG](https://gitlab.com/tno-tsg) Gitlab group.

## Features

The main features of the TNO Security Gateway are of course the pillars of the [International Data Spaces](https://internationaldataspaces.org/) movement:

* **Trust**: Trust is the basis of International Data Spaces. This is implemented by means of evaluation and certification before getting granted access to the dataspace.
* **Security & Data Sovereignty**: Secure data exchanging with the certainty of remaining in control of your data, by means of security protocols and policy enforcement.
* **Ecosystem of Data**: Decentralization is a key aspect of International Data Spaces, with the principle of keeping the data as close to the source as possible and only sharing it when explicitly allowed.
* **Standardized Interoperability**: Standardized communication patterns between IDS Connectors allow the creation and the interoperability of different connector implementations, of which the TSG is one.
* **Value Adding Apps**: The ability to create a modular ecosystem in which value-adding applications can be added on-demand to IDS connectors.
* **Data Markets**: Data-driven services combined with the pillars above allow for new business models to thrive.

But next to these pillars, the TSG components offer a unique set of features:

* **Kubernetes support**: All of the TSG components are created with support for Kubernetes in mind. This makes the deployments of components robust and kickstarts production-ready deployments.
* **Abstractions of IDS specifics**: By means of specific Data Apps developed to enable the inclusion of existing systems.
* **Embedded Policy Enforcement**: The TSG Core Container contains an embedded Policy Enforcement Framework that supports a variety of IDS Usage Policies.
* **Easy Configuration**: The TSG components can be deployed in a wide variety of use cases. Configuration of the components receives special care. This prevent overwhelming configuration effort, while it still allows enough flexibility of the configuration.

## Getting started

This documentation is aimed to support readers who want to get more information about IDS and TSG. Ranging from deploying existing component to information on how to create new functionality.

The [**Communication**]({{ '/docs/communication' | relative_url }}) page is the starting point to get more information on how information can be shared with the TSG components.

The [**Deployment**]({{ '/docs/deployment' | relative_url }}) page describes how to deploy an instance of the TSG and participate in a dataspace.

The [**Core Container**]({{ '/docs/core-container' | relative_url }}) page contains information on the core component of the TSG, to get in-depth information on how to interact with the internal API and how to configure the Core Container.

The [**Data Apps**]({{ '/docs/core-container' | relative_url }}) page offers more information on the Data Apps and shows which Data Apps are available. It also shows how to create your own Data App, to add functionality that is not provided by one of the existing Data Apps.

## Component Overview

Deployments of the TSG components occurs within an IDS-based dataspace. The figure below shows the required and optional components in a fully operational dataspace, where there are generic dataspace enabling services (Identity Provider, Metadata Broker, Clearing House, Vocabulary Hub, App Store).

The following paragraphs provide a short summary of each component.

### Connector / TNO Security Gateway

The connector is the primary point of communication within the dataspace, all communication with other parties is executed by or managed by the connector. The TSG Core Container is an implementation the core services of a connector, accompanied with additional services (data apps) is the connector. The IDS information model is the primary ontology that is used for supplementing requests and responses with metadata on the message so that its receiver is able to deduce the context and purpose of the message. The connector is responsible for making sure usage policies are enforced, either by using an internal policy enforcement framework (PEF) or by using an external services that handles the policies. For all messages the connector exchanges a Dynamic Attribute Token (DAT) must be included in that proves certain attributes for the connector are valid and are claimed by the Dynamic Attribute Provisioning Service (DAPS). Examples of these attributes are the certification level, attested capabilities, the transport certificate fingerprint of the connector.

Currently, there isn't a standardized Data App structure yet in the IDS specifications. Therefore, within the TSG the choice is made to implement a proprietary data app structure. These data apps are designed to handle IDS messages with the accompanying metadata directly. This way no specific logic is needed in the core container to connect a new data app to the core container. As long as the data app is able to handle IDS messages, the core container can include the data app. In this architecture the core container is responsible for verifying the DAT and policies for the request, while the data app itself will handle the business logic related to the message. The core container, however, is also capable of handling artifacts without the need for a data app. This makes the core container on its own compatible with other connector implementations (such as the [Dataspace Connector](https://international-data-spaces-association.github.io/DataspaceConnector/){:target="_blank"}).

> See: [IDS RAM 4.0 - 3.5.2 - IDS Connector](https://docs.internationaldataspaces.org/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer/3_5_2_ids_connector){:target="_blank"}  
> See: [TSG - Core Container]({{ '/docs/core-container' | relative_url }})  
> See: [TSG - Data Apps]({{ '/docs/data-apps' | relative_url }})

### Identity Provider

The Identity Provider is an essential enabling service for a dataspace, as it provides identities as well as the Dynamic Attribute Tokens for continiously verifying the claims attached to the identities. The Identity Provider is a combination of a Certificate Authority (CA) and a Dynamic Attribute Provisioning Service (DAPS). The Certificate Authority is based on a X.509 scheme with one or multiple CA certificates that sign public keys of components, participants, or devices. The certificates signed by the CA are usually only used for signing, separate certificates are used for transport (e.g. for encrypting TLS tunnels and HTTPS connections) to allow certificates that are more easily accepted by other applications (e.g. by using LetsEncrypt certificates). The different certificates must be linked to ensure the used transport certificates really belongs to the identity that it claims to be. This is done via the DAPS, that provides short-lived tokens in order to create a more dynamic identity scheme by including information that can change within the usual validity of the X.509 certificate (2 year). The connector uses its certificate to send an client assertion to the DAPS, which in turn will create the DAT based on the identity that signed the client assertion. Therefore, the DAT is the primary piece of data that provides the value and trust to the remote party that validates the incoming request.

The TSG implementation of the Identity Provider combines the CA and DAPS functionality in one software solution. This choice makes it easy to request certificates by means of Certificate Signing Requests (CSRs) that are included linked in the DAPS to provide the necessary information needed for creating the DATs. The Identity Provider implementation will soon be made open-source.

> See: [IDS RAM 4.0 - 3.5.1 - Identity Provider](https://docs.internationaldataspaces.org/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer/3_5_1_identity_provider){:target="_blank"}

### Metadata Broker

The Metadata Broker provides the yellow pages of a dataspace, by providing the metadata of connected connectors to provide discoverability within the dataspace. Though the Metadata Broker is an optional, enabling service for a dataspace, it is used in most dataspaces. Connectors in the dataspace are responsible for making sure the information in the metadata broker is up-to-date by informing the metadata broker of updates. This also means that connectors can decide what information they share with the publically visible broker (at least visible for other connectors in the dataspace). The information stored in the metadata broker can be retrieved by connectors by querying (via e.g. SPARQL) or by browsing a directory of connectors.

The metadata broker itself is implemented in the form of a connector. In the TSG design, the broker functionality is contained in a data app, with a triple store as backend to enable the SPARQL query functionality. Since the messages the metadata broker needs to handle are IDS Information Model annotated messages. The implementation of the data app will be made open-source soon. It can be combined with the TSG Core Container and a triple store (e.g. Apache Fuseki) to deploy a full instance of a Metadata Broker.

> See: [IDS RAM 4.0 - 3.5.4 - Metadata Broker](https://docs.internationaldataspaces.org/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer/3_5_4_metadata_broker){:target="_blank"}

### Clearing House

The Clearing House is an optional service for a dataspace, that allows a trusted third-party to provide secure logging functionality. Especially logging on the aspect of clearing & settlement and to provide external logging for the execution of usage control.

Currently, there is no implementation of a clearing house available within the TSG. But there is an implementation available in the [IDSA Github - Clearing House](https://github.com/International-Data-Spaces-Association/ids-clearing-house-service) that is maintained by Fraunhofer AISEC. The integration between the TSG Core Container and its Policy Enforcement Framework with this clearing house implementation is not formalized yet and no examples are available yet.

> See: [IDS RAM 4.0 - 3.5.5 - Clearing House](https://docs.internationaldataspaces.org/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer/3_5_5_clearing_house){:target="_blank"}

### Vocabulary Hub

The Vocabulary Hub is an optional service for a dataspace, that allows for management and retrieval of vocabularies used in the dataspace. An example of such a vocabulary is the IDS Information Model, containing the vocabulary of all the metadata used within IDS. Next to the generic vocabulary, the vocabulary hub can also contain domain-specific vocabularies that can be used to describe the actual data that is transferred with connectors.

Currently, there is no implementation of a vocabulary hub yet. But [Semantic Treehouse](https://www.semantic-treehouse.nl/){:target="_blank"} is working on the development of such a component, with next to the management and retrieval of vocabularies also support for tooling around mapping messages of existing applications towards vocabularies (e.g. with [RML](https://rml.io){:target="_blank"}). A position paper is released that describes how Semantic Treehouse could provide the vocabulary hub functionality, the position paper can be retrieved here: [The Vocabulary Hub to configure data space connectors](https://www.semantic-treehouse.nl/assets/files/04_TNO-april-2022-The-Vocabulary-Hub-to-configure-data-space-connectors-b0c0ba64f3f915894003c331de9cca50.pdf){:target="_blank"}.

> See: [IDS RAM 4.0 - 3.5.6 - Vocabulary Hub](https://docs.internationaldataspaces.org/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer/3_5_6_vocabulary_hub){:target="_blank"}

### App Store

The App Store is an optional service for a dataspace, that provides a secure registry of data apps. Which is especially useful when certified data apps are available, as the app store can provide this data app and its relevant metadata (including the certificate) to a connector.

Currently, there is no implementation of an app store that is compatible with the TSG components. A reason for this is that the current data app structure used in the TSG Core Container does not work together with the generic data apps as proposed in the working versions of the IDS specifications. The expected major update of the IDS specification will feature a separate control plane and data plane. As the data apps are likely to be largely within the data plane, changes are expected with regards to the app store and data apps. Therefore, the focus for the TSG is, for now, not on implementing an app store in the near future.

> See: [IDS RAM 4.0 - 3.5.3 - App Store and App ecosystem](https://docs.internationaldataspaces.org/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer/3_5_3_app_store_and_data_apps){:target="_blank"}
