The _TNO Security Gateway (TSG)_ is an open-source IDS connector implementation, initially developed at TNO. This site provides explanations of the high-level architecture and the in-depth documentation of the TSG.

The [TSG Gitlab group](https://gitlab.com/tno-tsg) contains several building blocks, this documentation provides an overarching view over the different building blocks.

This site is divided into [Architecture](/pages/architecture) and [Documentation](/docs/overview), containing respectively the high-level architecture together with use cases and in-depth documentation of the different components of the TSG.
