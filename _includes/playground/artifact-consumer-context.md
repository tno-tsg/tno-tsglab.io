The consumption of artifacts is the default IDS way of interaction between two connectors. Following the general process of:
1. retrieving the metadata of the connector and artifact, 
2. negotiating a contract for the artifact, 
3. retrieving the artifact. 

The test connector deployed at [https://test-connector.playground.dataspac.es/ui/](https://test-connector.playground.dataspac.es/ui/){:target="_blank"} provides a single artifact with a simple policy offer attached to it that allows for `READ` and `USE` rights if a consumer requests a contract with a validity in 2022 and 2023.

{% if include.type == "overview" %}
The following deployment strategies can be chosen for consuming artifacts:
* [Local Kubernetes]({{ '/playground/consumer/artifact/local' | relative_url }}): For Kubernetes clusters on local hardware, e.g. via Docker Desktop, Minikube, MicroK8s.
* [Public Kubernetes]({{ '/playground/consumer/artifact/public' | relative_url }}): For Kubernetes clusters on cloud infrastructure or on self-managed servers that are reachable from the internet.
* [Docker Compose]({{ '/playground/consumer/artifact/docker' | relative_url }}): If you don't have any experience with Kubernetes and no intention to use it in the future. Configuration will be harder for Docker deployments, especially when moving towards a publicly available connector.
{% endif %}