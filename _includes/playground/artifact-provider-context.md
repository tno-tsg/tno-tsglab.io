Providing artifacts is the default IDS way of interaction between two connectors. Following the general process of:
1. uploading an artifact to your local connector including metadata and optionally a contract offer, 
2. publishing the metadata of the artifact to the metadata broker,
3. perform (automatic) policy negotiation on receival of contract requests,
3. providing the artifact. 

The test connector deployed at [https://test-connector.playground.dataspac.es/ui/](https://test-connector.playground.dataspac.es/ui/){:target="_blank"} can be used as consumer for the artifact that is deployed on your connector.
