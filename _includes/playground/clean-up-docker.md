
## Clean-up

If you do not want to proceed with other tutorials, you can remove the resources used.

To clean-up all resources of the playground execute, in the same folder as the `docker-compose.yml` file:

```bash
docker-compose down
```