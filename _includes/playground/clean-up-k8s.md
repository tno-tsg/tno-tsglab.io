
## Clean-up

If you do not want to proceed with other tutorials, you can remove the resources used.

To remove all of the used Kubernetes resources execute:

```bash
helm uninstall playground
kubectl delete secret/ids-identity-secret
```