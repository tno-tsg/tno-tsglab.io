
## Configuration

* **_Download templates_**  
  - `docker-compose.yml`: [{{"/assets/playground/docker-compose.yml" | absolute_url}}]({{"/assets/playground/docker-compose.yml" | absolute_url}}){:download}
  - `application.yaml`: [{{"/assets/playground/application.yaml" | absolute_url}}]({{"/assets/playground/application.yaml" | absolute_url}}){:download}
* **_Provide IDS Identity_**  
  Configure the certificates that you've created in the [**Request Playground Identities**](#request-playground-identities) step.
  * Make sure the following files are in the same folder as `docker-compose.yml`: `cachain.crt`, `component.crt`, `component.key` with the PEM-based CA chain, public and private keys
  * Modify `info.idsid`, `info.curator`, `info.maintainer` to the corresponding identifiers that you filled in during creation of the certificates. `info.idsid` should be the Connector ID, and `info.curator`, `info.maintainer` should be the Participant ID.
* **_Change Admin User credentials_**  
  The default password is `playground` but this can be changed to a more secure password, which is especially needed for public Kubernetes deployments.
  * Modify `security.users[0].password` to a new BCrypt encoded password (e.g. via [https://bcrypt-generator.com/](https://bcrypt-generator.com/))
{% if include.openapi -%}
* **_Enable OpenAPI data app_**
  * Uncomment the OpenAPI block at the bottom of the template.
{% endif %} 