
## Configuration
* **_Download templates_**  
  {% if include.public -%}
  - `values.public.yaml`: [{{"/assets/playground/values.public.yaml" | absolute_url}}]({{"/assets/playground/values.public.yaml" | absolute_url}}){:download}
  {% else -%}
  - `values.local.yaml`: [{{"/assets/playground/values.local.yaml" | absolute_url}}]({{"/assets/playground/values.local.yaml" | absolute_url}}){:download}
  {% endif %}
* **_Create IDS Identity secret_**  
  Create an Kubernetes secret containing the certificates that you've created in the [**Request Playground Identities**]({{"/playground/identities" | absolute_url}}) step. 
  ```bash
  kubectl create secret generic ids-identity-secret --from-file=ids.crt=./component.crt --from-file=ids.key=./component.key --from-file=ca.crt=./cachain.crt
  ```
  * Modify `ids.info.idsid`, `ids.info.curator`, `ids.info.maintainer` in the `values.local.yaml` file to the corresponding identifiers that you filled in during creation of the certificates. `ids.info.idsid` should be the Connector ID, and `ids.info.curator`, `ids.info.maintainer` should be the Participant ID.
* **_Change Admin User credentials_**  
  The default password is `playground` but this can be changed to a more secure password, which is especially needed for public Kubernetes deployments.
  * Modify `security.users[0].password` to a new BCrypt encoded password (e.g. via [https://bcrypt-generator.com/](https://bcrypt-generator.com/))

{% if include.public %}
* **_Make the connector accessible for others_**  
  To make the connector available for other connectors Ingresses are used.
  * Modify `host` to the domain name you configured with the ingress controller
  * (Optionally) Set `useNewIngress` to `false` in case you're using an older version of Kubernetes (<=v1.21)
  * (Optionally) Modify `coreContainer.ingress.clusterIssuer`, `coreContainer.ingress.secretName`, and/or `coreContainer.ingress.annotations` to change the behaviour of the main Ingress
  * (Optionally) Modify `adminUi.ingress.clusterIssuer`, `adminUi.ingress.secretName`, and/or `adminUi.ingress.annotations` to change the behaviour of the admin UI Ingress

{% endif %}
{% if include.openapi -%}
* **_Enable OpenAPI data app_**
  * Uncomment the OpenAPI block at the bottom of the template.
{% endif %} 