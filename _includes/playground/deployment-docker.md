
## Deployment

To deploy the TSG Core Container execute:

```bash
docker-compose up -d
```

The containers will be deployed in detached mode, the user interface of the TSG Core Container will be available at: [http://localhost:31000](http://localhost:31000){:target="_blank"}

To follow the logs of the TSG Core Container execute:

```bash
docker-compose logs -f tsg-core-container
```
