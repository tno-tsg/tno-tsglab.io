
## Deployment

For this the Helm repository must be added:

```bash
helm repo add tsg https://nexus.dataspac.es/repository/tsg-helm
helm repo update
```

{% if include.type == "local" %}

After that the Helm chart must be installed, for the local Kubernetes version:
```bash
helm upgrade --install playground tsg/tsg-connector --version 3.0.0-master -f values.local.yaml
```

The user interface of the TSG Core Container will be available at: [http://localhost:31000](http://localhost:31000){:target="_blank"}

{% else %}
After that the Helm chart must be installed, for the public Kubernetes version:
```bash
helm upgrade --install playground tsg/tsg-connector --version 3.0.0-master -f values.public.yaml
```

The user interface of the TSG Core Container will be available at the hostname you've configured, on the path `/ui/` (e.g. https://domain.name/ui/)

{% endif %}

To view the deployment and show the logs of the TSG Core Container, either [_Lens_](https://k8slens.dev/){:target="_blank"} or _kubectl_ can be used.
For Lens, the main view will be _Workloads > Pods_. For kubectl the following commands can be used to view information of the deployment:
```bash
kubectl get pods
kubectl logs -f deployment/playground-tsg-connector-core-container
```
