The consumption of API endpoints is an addition to the default IDS interaction that allows to exchange API endpoints. This allows for existing HTTP services to work over IDS. For this the [OpenAPI Data App]({{ '/docs/data-apps/existing/#openapi-data-app' | relative_url }}) is used.

The process of consuming an API is as follows:
1. retrieve metadata of the connector and OpenAPI data app resources,
2. send the HTTP request to the OpenAPI data app connected to your own connector

The test connector deployed at [https://test-connector.playground.dataspac.es/ui/](https://test-connector.playground.dataspac.es/ui/){:target="_blank"} provides an agent that exposes an test server implementing the [https://httpbin.org/](https://httpbin.org/) API specification.

{% if include.type == "overview" %}
The following deployment strategies can be chosen for consuming API endpoints:
* [Local Kubernetes]({{ '/playground/consumer/openapi/local' | relative_url }}): For Kubernetes clusters on local hardware, e.g. via Docker Desktop, Minikube, MicroK8s.
* [Public Kubernetes]({{ '/playground/consumer/openapi/public' | relative_url }}): For Kubernetes clusters on cloud infrastructure or on self-managed servers that are reachable from the internet.
* [Docker Compose]({{ '/playground/consumer/openapi/docker' | relative_url }}): If you don't have any experience with Kubernetes and no intention to use it in the future. Configuration will be harder for Docker deployments, especially when moving towards a publicly available connector.
{% endif %}