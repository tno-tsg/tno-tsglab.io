## OpenAPI Data App

An OpenAPI Data App is deployed on [https://test-connector.playground.dataspac.es](https://test-connector.playground.dataspac.es/ui/). This OpenAPI Data App forwards the requests that are sent to it a service implementing the [https://httpbin.org/](https://httpbin.org/) API specification. 

To test if the deployment worked correctly, visit  [{{ include.base }}/#/]({{ include.base }}/#/){:target="_blank"}. This contains a very simple form where you can enter a message which is sent to the httpbin OpenAPI data app. If everything is setup correctly, you should see the message you sent being returned in a response.

### Deploy your own client

If you want, you can also write a custom client app that communicates with httpbin. For this, you only have to change the image name in the httpbin-ui container. Make sure that your application adds the `FORWARD_TO`, `FORWARD_SENDER`, `FORWARD_RECIPIENT`, `FORWARD_ACCESSURL` and `API_BACKEND` headers to its outgoing requests.

This step is especially useful if you are going to [provide an API]({{ '/playground/provider/openapi' | relative_url }}).