Providing API endpoints is an addition to the default IDS interaction that allows to exchange API endpoints. This allows for existing HTTP services to work over IDS. For this the [OpenAPI Data App]({{ '/docs/data-apps/existing/#openapi-data-app' | relative_url }}) is used.

The process of providing an API is as follows:
1. configure/deploy the backend service implementing an OpenAPI specification,
2. publish the metadata of the service to the metadata broker,
3. perform (automatic) policy negotiation on receival of contract requests,
4. forward the request to the backend service,
5. exchange the response to the request to the requestor.

The test connector deployed at [https://test-connector.playground.dataspac.es/ui/](https://test-connector.playground.dataspac.es/ui/){:target="_blank"} can be used as consumer for the artifact that is deployed on your connector.
