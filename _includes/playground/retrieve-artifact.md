## Request artifact

An artifact is provided by a connector deployed on [https://test-connector.playground.dataspac.es](https://test-connector.playground.dataspac.es/ui/){:target="_blank"}. The following steps show how this artifact can be retrieved from this connector.

The steps that must be followed are:
1. Retrieve metadata of the artifact.  
   See [Self Description Message Flow]({{ '/docs/communication/message-flows/#self-description' | relative_url }}){:target="_blank"}
2. Perform the contract negotiation process to establish a contract agreement.  
   See [Policy Negotiation Message Flow]({{ '/docs/communication/message-flows/#policy-negotiation' | relative_url }}){:target="_blank"}
3. Retrieve the artifact.  
   See [Artifact Request Message Flow]({{ '/docs/communication/message-flows/#artifact-request' | relative_url }}){:target="_blank"}

### Retrieve metadata of the artifact

Navigate to [{{ include.base }}/#/selfdescription/request]({{ include.base }}/#/selfdescription/request){:target="_blank"}

Fill in the form with the following values:

|---|---|
| _Connector ID_ | `urn:playground:tsg:connectors:TestConnector` |
| _Agent ID_ | `leave empty` |
| _Access URL_ | `https://test-connector.playground.dataspac.es/selfdescription` |
| _Requested Element_ | leave empty |

And click `Request description`.

When the self description of the connector is retrieved successfully, the details are shown in a tabular form.

Click the catalog `urn:playground:tsg:connectors:TestConnector:resources` to show the available resources.

Click on the lock icon under _Actions_ to go the next step directly.

### Perform the contract negotiation process to establish a contract agreement

If you've clicked the lock icon in the previous step, the values in the form are filled in automatically. Otherwise, navigate to [{{ include.base }}/#/artifacts/consumer]({{ include.base }}/#/artifacts/consumer){:target="_blank"} and the following values can be used:
<table>
  <tbody>
    <tr>
      <td><em>Connector ID</em></td>
      <td><code class="language-plaintext highlighter-rouge">urn:playground:tsg:connectors:TestConnector</code></td>
    </tr>
    <tr>
      <td><em>Agent ID</em></td>
      <td><code class="language-plaintext highlighter-rouge">leave empty</code></td>
    </tr>
    <tr>
      <td><em>Access URL</em></td>
      <td><code class="language-plaintext highlighter-rouge">https://test-connector.playground.dataspac.es/router/artifacts/urn%3Aplayground%3Atsg%3Aconnectors%3ATestConnector%3Aresources%3Aa3ea77e5-e786-4b5b-a637-4467c029df2e</code></td>
    </tr>
    <tr>
    <td><em>Contract Offer</em></td>
    <td><details markdown="1">
  <summary>Contract Offer JSON-LD</summary>
  ```json
  {
    "@type": "ids:ContractOffer",
    "@id": "https://w3id.org/idsa/autogen/contractOffer/211791f1-7ce6-4e01-b198-6abfcdd97719",
    "ids:permission": [
        {
        "@type": "ids:Permission",
        "@id": "https://w3id.org/idsa/autogen/permission/2441e172-574e-4bf5-b78b-51325b825253",
        "ids:action": [
            {
            "@id": "https://w3id.org/idsa/code/READ"
            },
            {
            "@id": "https://w3id.org/idsa/code/USE"
            }
        ],
        "ids:target": {
            "@id": "urn:playground:tsg:connectors:TestConnector:artifacts:9aa27c59-9f0c-4e77-9ad5-420f5eda0b98"
        }
        }
    ],
    "ids:contractStart": {
        "@value": "2022-01-01T00:00:00.000Z",
        "@type": "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
    },
    "ids:contractEnd": {
        "@value": "2023-12-31T00:00:00.000Z",
        "@type": "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
    }
  }
  ```
  </details></td>
</tr>
  </tbody>
</table>


Click `Request Contract` to perform the contract negotiation process.

If the process is successfully completed, the _Transfer Contract_ field in the _Request Artifact_ form should be filled with an URL similar to `https://w3id.org/idsa/autogen/contractAgreement/00000000-0000-0000-0000-000000000000`

### Retrieve the artifact

If you've just executed the contract negotiation process, the _Request Artifact_ is filled in automatically. Otherwise, navigate to [{{ include.base }}/#/artifacts/consumer]({{ include.base }}/#/artifacts/consumer){:target="_blank"} and use the following values:

|---|---|
| _Artifact ID_ | `urn:playground:tsg:connectors:TestConnector:artifacts:9aa27c59-9f0c-4e77-9ad5-420f5eda0b98` |
| _Connector ID_ | `urn:playground:tsg:connectors:TestConnector` |
| _Agent ID_ | `urn:playground:tsg:TNO` |
| _AccessURL_ | `https://test-connector.playground.dataspac.es/router/artifacts/urn%3Aplayground%3Atsg%3Aconnectors%3ATestConnector%3Aresources%3Aa3ea77e5-e786-4b5b-a637-4467c029df2e` |
| _Transfer Contract_ | navigate to [{{ include.base }}/#/pef/pap]({{ include.base }}/#/pef/pap){:target="_blank"} and copy the _Agreement ID_. If it's automatically filled in, you don't have to copy the ID from the PAP. |

Click `Request Artifact` to actually download the artifact to your local system.
