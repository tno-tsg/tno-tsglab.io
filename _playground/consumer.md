---
layout: playground
title: Consumer
toc: true
left_menu: true
slug: consumer
---

The consumer part of the TSG Playground consists out of two types of interactions:
- Consuming artifacts
- Consuming API endpoints

Since for the consumption of resources it is not required for the consuming connector to be available on the internet, there are three different deployment variants of the connector. Either via Kubernetes, preferred since this allows to easily migrate to a deployment that is reachable by others, or via Docker-compose.

## Consuming artifacts

{% include playground/artifact-consumer-context.md type="overview" %}

## Consuming API endpoints

{% include playground/openapi-consumer-context.md type="overview" %}

