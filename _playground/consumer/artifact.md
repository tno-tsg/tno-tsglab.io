---
layout: playground
title: Consume Artifact
toc: false
left_menu: true
categories: consumer
slug: artifact
---

{% include playground/artifact-consumer-context.md type="overview" %}
