---
layout: playground
title: Consume Artifact via Docker Compose
toc: true
left_menu: true
categories: consumer artifact
slug: docker
---

{% include playground/artifact-consumer-context.md %}

{% include playground/config-docker.md %}

{% include playground/deployment-docker.md %}

{% include playground/retrieve-artifact.md base="http://localhost:31000" %}

{% include playground/clean-up-docker.md %}