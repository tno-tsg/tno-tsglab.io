---
layout: playground
title: Consume Artifact via Local Kubernetes
toc: true
left_menu: true
categories: consumer artifact
slug: local
---

{% include playground/artifact-consumer-context.md %}

{% include playground/config-k8s.md %}

{% include playground/deployment-helm.md type="local" %}

{% include playground/retrieve-artifact.md base="http://localhost:31000" %}

{% include playground/clean-up-k8s.md %}
