---
layout: playground
title: Consume Artifact via Public Kubernetes
toc: true
left_menu: true
categories: consumer artifact
slug: public
---

{% include playground/artifact-consumer-context.md %}

{% include playground/config-k8s.md public='' %}

{% include playground/deployment-helm.md type="public" %}

{% include playground/retrieve-artifact.md base="https://domain.name/ui" %}

{% include playground/clean-up-k8s.md %}
