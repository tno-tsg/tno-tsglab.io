---
layout: playground
title: Consume API endpoints
toc: false
left_menu: true
categories: consumer
slug: openapi
---

{% include playground/openapi-consumer-context.md type="overview" %}
