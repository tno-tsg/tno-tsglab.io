---
layout: playground
title: Consume API endpoint via Docker Compose
toc: true
left_menu: true
categories: consumer openapi
slug: docker
---

{% include playground/openapi-consumer-context.md %}

{% include playground/config-docker.md openapi='' %}

{% include playground/deployment-docker.md %}

{% include playground/openapi-data-app.md base="http://localhost:31001" %}

{% include playground/clean-up-docker.md %}