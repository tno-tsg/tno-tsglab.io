---
layout: playground
title: Consume API endpoints via Local Kubernetes
toc: true
left_menu: true
categories: consumer openapi
slug: local
---

{% include playground/openapi-consumer-context.md %}

{% include playground/config-k8s.md openapi='' %}

{% include playground/deployment-helm.md type="local" %}

{% include playground/openapi-data-app.md base="http://localhost:31001" %}

{% include playground/clean-up-k8s.md %}