---
layout: playground
title: Consume API endpoints via Public Kubernetes
toc: true
left_menu: true
categories: consumer openapi
slug: public
---

{% include playground/openapi-consumer-context.md %}

{% include playground/config-k8s.md public='' openapi='' %}

{% include playground/deployment-helm.md type="public" %}

{% include playground/openapi-data-app.md base="https://domain.name/openapi-consumer" %}

{% include playground/clean-up-k8s.md %}