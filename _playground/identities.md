---
layout: playground
title: Create Identities
toc: false
left_menu: true
slug: identities
tags: playground, identities
---

You can request an test identity at the [TSG Playground Identity Provider](https://daps.playground.dataspac.es).
By navigating to Management and creating an account, you can request signed certificates with test certifications. There are two steps:
1. Request a participant certificate.
2. Request a connector certificate that is connected to the participant certificate.

For this you would need two keypairs, which can be created via OpenSSL or from within the user interface.

In the end, you should have a folder structure like:
~~~
.
├── participant.key
├── participant.crt
├── component.key
├── component.crt
└── cachain.crt
~~~

> The `cachain.crt` file can also be downloaded from: [{{"/assets/playground/cachain.crt" | absolute_url}}]({{"/assets/playground/cachain.crt" | absolute_url}}){:download}

These certificates can be used to interact within the TSG Playground dataspace, feel free to use these in the tutorials on this site. Usage outside of the tutorials is also allowed, but keep in mind that the identities are auto accepted. So they should only be used in early testing.

There is no guarentee that the service will be running the same over time, since updates will be pushed to the TSG Playground environment without notice. This may lead to certificates that previously did work will fail to do so at a later moment.