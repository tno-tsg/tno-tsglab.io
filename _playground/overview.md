---
layout: playground
title: TSG Playground
toc: true
left_menu: true
slug: overview
---

The TSG Playground provides the ability to easily test a TSG Core Container. The TSG Playground consists out of an Identity Provider, including Dynamic Attribute Provisioning Service, a Metadata Broker, and a Test Connector.

## Overview

An overview of the TSG playground is shown below, with high-level interaction patterns for the consumer (Cn.) and provider (Pn.)

![Playground overview - Click to enlarge]({{"/assets/images/drawio/playground.drawio.svg" | absolute_url}}){:.image-modal}
<center><strong>Playground overview</strong></center>

The deployed components are accessible from the internet:
- **TSG Playground Identity Provider**: [https://daps.playground.dataspac.es](https://daps.playground.dataspac.es){:target="_blank"}
- **TSG Playground Metadata Broker**: [https://broker.playground.dataspac.es](https://broker.playground.dataspac.es){:target="_blank"}
- **TSG Playground Test Connector**: [https://test-connector.playground.dataspac.es/ui/](https://test-connector.playground.dataspac.es/ui/){:target="_blank"}

For testing in the TSG Playground, you'll have to register identities at the Identity Provider. See [Identities]({{ '/playground/identities' | relative_url }}) for a detailed description of how to create and register your test identity. In this setup, identities are automatically provided with a test certification, in order to make it as simple as possible to connect to the TSG Playground.

Different deployment strategies can be used to interact with the TSG Playground: Local Kubernetes, Public Kubernetes, Docker Compose. For consuming data from the Test Connector, all three of these strategies can be used. For providing data to the Test Connector, however, it is required to deploy your connector so that it is available on the internet. For this, only the Public Kubernetes variant is supported in documentation. You can achieve the same result with Local Kubernetes or Docker Compose, although it is much more complex and dependend on your environment.

The Test Connector supports both standard artifact exchanges as [OpenAPI data app]({{ '/docs/data-apps/existing#openapi-data-app' | relative_url }}) exchanges.

## How to start

The first thing you need to do if you want to use the TSG Playground is register your identity, go to [Identities]({{ '/playground/identities' | relative_url }}) and create a participant and component certificate.

After you've registered your identity, the next logical step will be to [consume an artifact]({{ '/playground/consumer/artifact' | relative_url }}) from the Test Connector. Using Kubernetes is preferred, either locally via e.g. Docker-Desktop or publically on a hosted Kubernetes cluster by a cloud provider or on your own server. Docker compose is also supported, but not recommended since you won't be able to easily deploy data apps and make your deployment available on the internet.

After consuming an artifact, either progress to [consume an API]({{ '/playground/consumer/openapi' | relative_url }}) via the OpenAPI data app or [provide an artifact]({{ '/playground/provider/artifact' | relative_url }}) to the Test Connector.