---
layout: playground
title: Provider
toc: true
left_menu: true
slug: provider
---

The provider part of the TSG Playground consists out of two types of interactions:
- Providing artifacts
- Providing API endpoints

For providing resources to other connectors it is required that the connector is deployed in way that it is reachable from the internet. Therefore, the tutorials are focussed on Public Kubernetes deployments.

## Providing artifacts
{% include playground/artifact-provider-context.md %}

## Providing API endpoints
{% include playground/openapi-provider-context.md %}
