---
layout: playground
title: Provide Artifact
toc: true
left_menu: true
categories: provider
slug: artifact
---

{% include playground/artifact-provider-context.md %}

{% include playground/config-k8s.md public='' %}

{% include playground/deployment-helm.md type="public" %}

## Providing artifact

To provide an artifact that can be retrieved by other connectors only a single form is required to be filled in.

1. Navigate to [https://domain.name/ui/#/artifacts/provider](https://domain.name/ui/#/artifacts/provider){:target="_blank"}
2. Create a _Title_ and _Description_ for your artifact
3. Select the file you want to provide
4. Leave the _Artifact ID_ empty
5. Click on _Generic read access_ to generate a contract offer that allows read access to consumers that do perform the contract negotiation process

## Consume provider artifact

### Retrieve metadata of the artifact

Navigate to [https://test-connector.playground.dataspac.es/ui/#/selfdescription/request](https://test-connector.playground.dataspac.es/ui/#/selfdescription/request){:target="_blank"} and login with the following credentials:
  - Username: `consumer`
  - Password: `TNOTSGPlayground`

Fill in the form with the relevant information:
  - _Connector ID_: The connector ID of the connector that you've deployed
  - _Agent ID_: The participant ID that you've created
  - _Access URL_: The publicly available access URL of your connector, e.g. `https://domain.name/selfdescription`
  - _Requested Element_: leave empty

And click `Request description`.

When the self description of the connector is retrieved successfully, the details are shown in a tabular form.

Click the `urn:playground:tsg:connectors:TestConsumer:resources` to show the available resources.

Click on the lock icon under _Actions_ to go the next step directly.

### Perform the contract negotiation process to establish a contract agreement

If you've clicked the lock icon in the previous step, the values in the form are filled in automatically. Otherwise, navigate to [https://test-connector.playground.dataspac.es/ui/#/artifacts/consumer](https://test-connector.playground.dataspac.es/ui/#/artifacts/consumer){:target="_blank"} and fill in the `Request Contract` form:
  - _Connector ID_: The connector ID of the connector that you've deployed
  - _Agent ID_: The participant ID that you've created
  - _Access URL_: The publicly available artifact access URL of your connector, e.g. `https://domain.name/router/artifacts`
  - _Contract Offer_: Copy the contract offer as provided in the metadata of the artifact

Click `Request Contract` to perform the contract negotiation process.

If the process is successfully completed, the _Transfer Contract_ field in the _Request Artifact_ form should be filled with an URL similar to `https://w3id.org/idsa/autogen/contractAgreement/00000000-0000-0000-0000-000000000000`.

> The `consumer` user does not have permission to list the contracts that are created on his behalf, so note down the contract identifier if you will perform the last step at a later moment.

### Retrieve the artifact

If you've just executed the contract negotiation process, the _Request Artifact_ is filled in automatically. Otherwise, navigate to [https://test-connector.playground.dataspac.es/ui/#/artifacts/consumer](https://test-connector.playground.dataspac.es/ui/#/artifacts/consumer){:target="_blank"} and use the following values:
- _Artifact ID_: `urn:playground:tsg:connectors:TestConsumer:artifacts:9ff5d13e-bab3-4660-bedc-efc01dfaeaad`
- _Connector ID_: `urn:playground:tsg:connectors:TestConsumer`
- _Agent ID_: `urn:playground:tsg:TNO`
- _AccessURL_: `https://test-connector.playground.dataspac.es/router/artifacts/urn:playground:tsg:connectors:TestConsumer:resources:c2212225-a86e-4438-abf3-a6758e0ce689`
- _Transfer Contract_: Copy the contract agreement ID of the previous step, if it is not filled in yet.

Click `Request Artifact` to actually download the artifact to your local system.

{% include playground/clean-up-k8s.md %}
