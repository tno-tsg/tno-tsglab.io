---
layout: playground
title: Provide API endpoints
toc: true
left_menu: true
categories: provider
slug: openapi
---

{% include playground/openapi-provider-context.md %}

{% include playground/config-k8s.md public='' %}

{% include playground/deployment-helm.md type="public" %}

## Providing an API

To provide an API that can be open to other connectors via the OpenAPI data app, you need an [OpenAPI specification](https://swagger.io/specification/). This specification needs to be hosted so that the consumer OpenAPI data app can view the specification at all times. Our recommendation for hosting your OpenAPI specification is [Swaggerhub](https://app.swaggerhub.com/). 

The first thing we are going to have to change is creating a route in our connector for the OpenAPI data app. This can be done by copying the following snippet in the `ids` block of the yaml file:

```yaml
  routes:
    ingress:
      http:
        - endpoint: router
          dataApp: http://{{ template "tsg-connector.fullname" . }}-openapi-data-app-http:8080/router
          parameters: "&matchOnUriPrefix=true"
```

Copy the following code snippet to the bottom of the `values.public.yaml` file, and make changes where they are requested:

```yaml
containers:
  - type: data-app
    name: openapi-data-app
    image: docker.nexus.dataspac.es/data-apps/openapi-data-app:feature-infomodel-4.2.7
    apiKey: APIKEY-4DVuWn93ijEo5ZucLgiR # CHANGE
    config:
      openApi:
        usePolicyEnforcement: false
        openApiBaseUrl: <Your openapi spec>
        versions:
          - 0.1.0
        agents:
          - id: <connector ID>:AgentA
            title: <connector name> Agent A
            backEndUrlMapping:
              0.1.0: http://{% raw %}{{ template "tsg-connector.fullname" . }}{% endraw %}-<your-api-name>-<service-name>
  - type: helper
    name: <your-api-name>
    image: <your image>
    services:
    - port: 80
      name: <service-name>
```

As before, change the API key to the previously set API key in `values.public.yaml`. Fill in the location of the OpenAPI specification in the openApiBaseUrl field. You can specify a version, which is 0.1.0 by default. To create an agent, create an agent ID. We recommend to use the connector ID with an agent ID suffix for this tutorial. The backEndUrlMapping can be done via the Pod name + the service name of the helper, preceded by `http://`.

More details on the configuration of the OpenAPI data app can be found on [Gitlab](https://gitlab.com/tno-tsg/data-apps/openapi)

{% include playground/clean-up-k8s.md %}