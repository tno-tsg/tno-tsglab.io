---
layout: doc
title: Architecture
toc: true
left_menu: false
slug: overview
---

Organizations increasingly collaborate in digital ecosystems, whilst being aware that data is becoming a key asset. They require improved data control capabilities that prevent their shared data from being misused.
The **_TNO Secure Gateway_ (TSG)** is based on the **_IDS Reference Architecture Model_ ([IDS-RAM](https://github.com/International-Data-Spaces-Association/IDS-RAM_4_0)) ([DIN SPEC 27070](https://internationaldataspaces.org/ids-is-officially-a-standard-din-spec-27070-is-published/))**.  Specifying rules and mechanisms ensuring data sovereignty within data ecosystems. The TSG follows the “security-by-design” concept in helping participants to become more sovereign. For example, to have the option to revoke consent.
The TSG connector are a crucial building block for setting up a data space. OPEN DEI has defined a data space as a ‘_decentralised infrastructure for trustworthy data sharing and exchange in data ecosystems based on commonly agreed principles_’

> This page is currently work in progress, more information will be added soon

## High-level overview

Three so-called _“quality attributes”_ are taken into account when designing the architecture.

1. **Security**: safe and trusted data sharing. All (meta) data is stored with the original source organization. And can be shared if desired depending on the authorization granted. The security architecture is divided into two parts: Organization/user, whereby a digital certificate must be issued at the organization level. During information sharing, participants of the ecosystem can identify themselves, whereby one or more identity providers (distribution of a digital certificate) can authenticate whether the participant is actually the participant it pretends to be. A so called trusted zone is available where all the entities are trusted to at least the level of the gateway.

2. **Compatibility**: It is important that information can be shared between parties in an easy & fast way. This implies the need for semantic interoperability between organizations with heterogeneous information systems to facilitate communication (the exchange of data); a common semantic concept.

3. **Portability**: For successful operation it is necessary that the ecosystem is scalable. This happens on several layers, namely ad-hoc participants (gateways) can be added or removed without affecting the functioning of the ecosystem. The architecture must also be scalable and flexible in order to be able to add new operational functionality in a fast and adequate manner. This is partly possible due to the current separation of functionality into modules. The architecture adheres to the “loosely-coupled” way of working. This means that all interfaces are standardized to increase scalability.

The current architecture supports a decentralized _(peer-to-peer)_ way of working together. All data is by standard stored (decentrally) at the source and can be shared via policies. If both the security requirements are met, combined with the correct authorization, the information can be shared securely. There is not one central node with all the data. This enforces data sovereignty at the data and metadata level. And it supports the operational functioning of the ecosystem: if one or more gateways unexpectedly fail, the other, still operational gateways, can continue to work together.



![Interaction of technical components - Click to enlarge]({{"/assets/images/IDS-RAM-3.5-interaction-of-technical-components.webp" | absolute_url}}){:.image-modal}
<center><strong>Interaction of technical components <sub>(source: <a href="https://docs.internationaldataspaces.org/ids-ram-4/layers-of-the-reference-architecture-model/3-layers-of-the-reference-architecture-model/3_5_0_system_layer" target="_blank">IDS RAM 4.0 - 3.5 - System layer</a>)</sub></strong></center>

## Information Layer

The information layer is one of the key components of the IDS architecture, which enables secure and trusted data exchange between different organizations in a decentralized and federated way. The information layer defines the core concepts, models, and protocols for managing and exchanging data in the IDS ecosystem.
 
The information layer consists of the following key components:
 
- Data Objects: Data objects represent a self-contained set of information that is stored and managed within an IDS connector or an IDS-enabled application. Each data object is identified by a unique identifier and contains metadata that describes the data, such as the data type, format, and access rights.
- Data Spaces: Data spaces are virtual containers that provide a logical and secure environment for storing and exchanging data objects. Each data space is controlled by an organization or a group of organizations that define the rules and policies for accessing and sharing the data objects within the space.
- Connector: Connectors are software components that enable organizations to connect to the IDS ecosystem and exchange data securely and reliably. Each connector implements the IDS communication protocols and security mechanisms, which ensure the confidentiality, integrity, and authenticity of the data exchange.
- Contracts: Contracts are formal agreements between different organizations that define the rules and policies for exchanging data within the IDS ecosystem. Each contract specifies the data objects, data spaces, and access rights that are granted to the participating organizations.
- Governance: Governance refers to the policies, processes, and mechanisms for managing and controlling the IDS ecosystem. The governance model of the IDS architecture is based on a federated approach, where each organization is responsible for managing its own data spaces and contracts, while adhering to the overall governance framework of the IDS ecosystem.
 
Overall, the information layer of the IDS architecture provides a standardized and interoperable framework for managing and exchanging data in a secure and trusted manner, which is essential for enabling digital business ecosystems and value networks.

## Security architecture

> Work in progress
